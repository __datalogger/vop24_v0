/**
 * ============================================================
 * @file apps.c
 * @author fede (rouxfederico@gmail.com)
 * @date 08/08/2019
 * ============================================================
 */

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "uart.h"

#include "sapi_sdcard.h"
#include "apps.h"

#include "ff.h"
#include "vop24v0_board.h"

#include "ads1292.h"
#include "timers.h"

#include <clock_17xx_40xx.h>




/*-------------------------- prototipos de funciones ------------------------------- */

/* tareas */
static void mdeConfiguracion (void* taskParam);
static void mdeInterfazTerminal (void *taskParam);

static void tInicializarADS1292 (void* taskParam) ;
static void tEnviarSgnPrueba (void*taskParam);
static int32_t InicializarIndicadorLed (void);
static void tIndicadorLed(void *pvParameters);

int32_t InicializarMDEInterfazTerminal (void);
int32_t InicializarMDEConfiguracion (void);

/* apps */
int32_t AppViaUartBLE_MLTBT05 (void);
int32_t AppLoopUarts (void);
int32_t AppTestSD (void);
int32_t AppTest_USBMSCD_SD (void);
int32_t AppEstadosCLI (void);

/* funciones */
int8_t CifraAsciiEntero (int8_t cifraAscii);

/* -------------------------- variables globales ------------------------------- */

#define SGN_PRUEBA_N 		128
#define SGN_PRUEBA_FS		100

uint16_t sgnPrueba[] = {
	#include "sin8hzFs100N128.h"
};

uint16_t calP0, calP1;
uint8_t canalSel = 3;
uint8_t fsSel;
uint32_t fsValor = 500;
uint8_t pgaCanal[2] = {6,6};


bool primerEntradaEstadoMDEConfiguracion = true;

estadoInterfazTerminal_t estadoInterfazTerminal;

static int8_t primerIngresoEstadoMDEIntefazTerminal = 1;
xSemaphoreHandle  semCanalBT;

sdcard_t sdcard;
ledEstados_t ledEstados;
xTaskHandle tInicializarADS1292_handle;
xTaskHandle USB_MSCD_Init_handler;

bool usbInicializado = false;


char FRstrings[20][23] = {
	"FR_OK",		/* (0) Succeeded */
	"FR_DISK_ERR",			/* (1) A hard error occurred in the low level disk I/O layer */
	"FR_INT_ERR",				/* (2) Assertion failed */
	"FR_NOT_READY",			/* (3) The physical drive cannot work */
	"FR_NO_FILE",				/* (4) Could not find the file */
	"FR_NO_PATH",				/* (5) Could not find the path */
	"FR_INVALID_NAME",		/* (6) The path name format is invalid */
	"FR_DENIED",				/* (7) Access denied due to prohibited access or directory full */
	"FR_EXIST",				/* (8) Access denied due to prohibited access */
	"FR_INVALID_OBJECT",		/* (9) The file/directory object is invalid */
	"FR_WRITE_PROTECTED",		/* (10) The physical drive is write protected */
	"FR_INVALID_DRIVE",		/* (11) The logical drive number is invalid */
	"FR_NOT_ENABLED",			/* (12) The volume has no work area */
	"FR_NO_FILESYSTEM",		/* (13) There is no valid FAT volume */
	"FR_MKFS_ABORTED",		/* (14) The f_mkfs() aborted due to any problem */
	"FR_TIMEOUT",				/* (15) Could not get a grant to access the volume within defined period */
	"FR_LOCKED",				/* (16) The operation is rejected according to the file sharing policy */
	"FR_NOT_ENOUGH_CORE",		/* (17) LFN working buffer could not be allocated */
	"FR_TOO_MANY_OPEN_FILES",	/* (18) Number of open files > FF_FS_LOCK */
	"FR_INVALID_PARAMETER"	/* (19) Given parameter is invalid */
	};


bool enviandoOnline = false;


/* -------------------------- implementacion de apps ------------------------------- */

/**
 * ----------------------------------------------------------------
 * @fn int32_t AppViaUartBLE_MLTBT05(void)
 * @brief aplicacion que hace un pasamanos entre la uart0 y la uart3
 * @brief esta pensado para trabajar con la placa MLT-BT05 y un
 * @brief conversor USB-TTL, los dos configurados a 115200bps. Si no
 * @brief funciona, probar con cambiar la velocidad de la uart BT
 * @brief a 9600bps.
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t AppLoopUarts (void) {

	/* inicializo pines de la placa */
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);

	/* inicializacion de la uart */
	UARTxInicializarEstructura(pUartBT, LPC_UART0);
	UARTxInicializarPines(pUartBT->pUART);
	UARTxInicializar115200bps8N1(pUartBT->pUART);
	UARTxHabilitarIRQ(pUartBT->pUART);

	/* inicializacion de la uart */
	UARTxInicializarEstructura(pUartDb, LPC_UART3);
	UARTxInicializarPines(pUartDb->pUART);
	UARTxInicializar115200bps8N1(pUartDb->pUART);
	UARTxHabilitarIRQ(pUartDb->pUART);

	/* LED1 tsoggle thread */
	xTaskCreate(tIndicadorLed, (signed char *) "Led",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 3UL),
				(xTaskHandle *) NULL);

	xTaskCreate(tTransmisionUART, (signed char *) "txUartBT",
					configMINIMAL_STACK_SIZE, (void*) pUartBT, (tskIDLE_PRIORITY + 2UL),
					(xTaskHandle *) NULL);

	xTaskCreate(tTransmisionUART, (signed char *) "txUartDb",
						configMINIMAL_STACK_SIZE, (void*) pUartDb, (tskIDLE_PRIORITY + 2UL),
						(xTaskHandle *) NULL);


	xTaskCreate(tLoopUART, (signed char *) "LoopBt",
						configMINIMAL_STACK_SIZE, (void*) pUartBT, (tskIDLE_PRIORITY + 1UL),
						(xTaskHandle *) NULL);

	xTaskCreate(tLoopUART, (signed char *) "LoopDb",
					configMINIMAL_STACK_SIZE, (void*) pUartDb, (tskIDLE_PRIORITY + 1UL),
					(xTaskHandle *) NULL);

	return 0;
}

/**
 * ----------------------------------------------------------------
 * @fn int32_t AppViaUartBLE_MLTBT05(void)
 * @brief aplicacion que hace un pasamanos entre la uart0 y la uart3
 * @brief esta pensado para trabajar con la placa MLT-BT05 y un
 * @brief conversor USB-TTL, los dos configurados a 115200bps. Si no
 * @brief funciona, probar con cambiar la velocidad de la uart BT
 * @brief a 9600bps.
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t AppViaUartBLE_MLTBT05 (void) {

	/* inicializo pines de la placa */
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);

	/* inicializacion de la uart */
	UARTxInicializarEstructura(pUartBT, LPC_UART0);
	UARTxInicializarPines(pUartBT->pUART);
	UARTxInicializar115200bps8N1(pUartBT->pUART);
	//Chip_UART_SetBaud(pUartBT->pUART, 9600);
	UARTxHabilitarIRQ(pUartBT->pUART);

	/* inicializacion de la uart */
	UARTxInicializarEstructura(pUartDb, LPC_UART3);
	UARTxInicializarPines(pUartDb->pUART);
	UARTxInicializar115200bps8N1(pUartDb->pUART);
	UARTxHabilitarIRQ(pUartDb->pUART);

	/* LED1 tsoggle thread */
	xTaskCreate(tIndicadorLed, (signed char *) "Led",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 3UL),
				(xTaskHandle *) NULL);

	xTaskCreate(tTransmisionUART, (signed char *) "txUartBT",
					configMINIMAL_STACK_SIZE, (void*) pUartBT, (tskIDLE_PRIORITY + 2UL),
					(xTaskHandle *) NULL);

	xTaskCreate(tTransmisionUART, (signed char *) "txUartDb",
						configMINIMAL_STACK_SIZE, (void*) pUartDb, (tskIDLE_PRIORITY + 2UL),
						(xTaskHandle *) NULL);

	xTaskCreate(tPasamanos, (signed char *) "via1",
						configMINIMAL_STACK_SIZE, (void*) viaUart1, (tskIDLE_PRIORITY + 2UL),
						(xTaskHandle *) NULL);

	xTaskCreate(tPasamanos, (signed char *) "via2",
						configMINIMAL_STACK_SIZE, (void*) viaUart2, (tskIDLE_PRIORITY + 2UL),
						(xTaskHandle *) NULL);

	return 0;
}



/**
 * ----------------------------------------------------------------
 * @fn int32_t AppTestSD(void)
 * @brief aplicacion de prueba de la SD
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t AppTestSD (void) {

	FIL fp;
	FRESULT fr;

	/* inicializo pines de la placa */
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);
	Board_SSP_Init(ADS1292_SSP_PORT);
	InicializarPinesVOP24v0_Board();

	SD_InicializarHardware();



	/* inicializacion de la uart para debug */
	UARTxInicializarEstructura(pUartDb, LPC_UART3);
	UARTxInicializarPines(pUartDb->pUART);
	UARTxInicializar115200bps8N1(pUartDb->pUART);

	UARTxHabilitarIRQ(pUartDb->pUART);


	uint8_t msg[64];

	Board_UARTPutSTR("\r\n");
	Board_UARTPutSTR("-------------------------------------------\r\n");
	Board_UARTPutSTR("Programa de prueba de la SD                \r\n");
	Board_UARTPutSTR("-------------------------------------------\r\n");

	Board_UARTPutSTR("Iniciando sd con configuracion:\r\n");
	sprintf((char*)msg, "vel inicial %iHz.\r\n", FSSDC_GetSlowClock());
	Board_UARTPutSTR((char*)msg);
	sprintf((char*)msg, "vel de trabajo %iHz.\r\n", FSSDC_GetFastClock());
	Board_UARTPutSTR((char*)msg);

	if(sdcardInit( &sdcard) == false)
	{
		Board_UARTPutSTR("Fallo inicio de sdcard.\r\nFIN\r\n");
		configASSERT(0);
	}
	else {
		Board_UARTPutSTR("Inicio sdcard ok. Unidad FatFs'");
		Board_UARTPutSTR(sdcardDriveName());
		Board_UARTPutSTR("'.\r\n");
	}

	fr = f_mount( &sdcard.fatFs, FF_VOLUME_STRS, 1);
	Board_UARTPutSTR("f_mount:");
	Board_UARTPutSTR(FRstrings[fr]);
	Board_UARTPutSTR("\r\n");

	if(fr!= FR_OK)
		configASSERT(0);

	Board_UARTPutSTR("\r\n");

	fr = f_open(&fp, "0:archivo.txt", FA_CREATE_ALWAYS | FA_WRITE);
	Board_UARTPutSTR("f_open:");
	Board_UARTPutSTR(FRstrings[fr]);
	Board_UARTPutSTR("\r\n");


	if(fr != FR_OK)
		configASSERT(0);
	int n;
	n = f_puts("prueba escribiendo", &fp);

	Board_UARTPutSTR("f_puts:");
	sprintf(msg, "escribio %d bytes.\r\n",n );
	Board_UARTPutSTR(msg);


	fr = f_close(&fp);
	Board_UARTPutSTR("f_close:");
	Board_UARTPutSTR(FRstrings[fr]);
	Board_UARTPutSTR("\r\n");



	Board_UARTPutSTR("Inicio parpadeo\r\n");



	/* LED1 tsoggle thread */
	xTaskCreate(tIndicadorLed, (signed char *) "Led",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 3UL),
				(xTaskHandle *) NULL);

	return 0;
}



/**
 * ----------------------------------------------------------------
 * @fn int32_t AppTestSD(void)
 * @brief aplicacion de prueba de la SD
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */
#include "msc_main.h"

int32_t AppTest_USBMSCD_SD (void) {

	uint8_t msg[64];

	FIL fp;
	FRESULT fr;

	/* inicializo pines de la placa */
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);
	Board_SSP_Init(ADS1292_SSP_PORT);
	InicializarPinesVOP24v0_Board();

	SD_InicializarHardware();

	/* inicializacion de la uart para debug */
	UARTxInicializarEstructura(pUartDb, LPC_UART3);
	UARTxInicializarPines(pUartDb->pUART);
	UARTxInicializar115200bps8N1(pUartDb->pUART);
	UARTxHabilitarIRQ(pUartDb->pUART);


	Board_UARTPutSTR("\r\n");
	Board_UARTPutSTR("-------------------------------------------\r\n");
	Board_UARTPutSTR("Programa de prueba de la SD                \r\n");
	Board_UARTPutSTR("-------------------------------------------\r\n");

	Board_UARTPutSTR("Iniciando sd con configuracion:\r\n");
	sprintf(msg, "vel inicial %iHz.\r\n", FSSDC_GetSlowClock());
	Board_UARTPutSTR(msg);
	sprintf(msg, "vel de trabajo %iHz.\r\n", FSSDC_GetFastClock());
	Board_UARTPutSTR(msg);

	if(sdcardInit( &sdcard) == false)
	{
		Board_UARTPutSTR("Fallo inicio de sdcard.\r\nFIN\r\n");
		configASSERT(0);
	}
	else {
		Board_UARTPutSTR("Inicio sdcard ok. Unidad FatFs'");
		Board_UARTPutSTR(sdcardDriveName());
		Board_UARTPutSTR("'.\r\n");
		sdcard.status = SDCARD_Status_ReadyMounted;
	}

	fr = f_mount( &sdcard.fatFs, FF_VOLUME_STRS, 1);
	Board_UARTPutSTR("f_mount:");
	Board_UARTPutSTR(FRstrings[fr]);
	Board_UARTPutSTR("\r\n");

	if(fr!= FR_OK)
		configASSERT(0);

	Board_UARTPutSTR("\r\n");

	fr = f_open(&fp, "0:archivo.txt", FA_CREATE_ALWAYS | FA_WRITE);
	Board_UARTPutSTR("f_open:");
	Board_UARTPutSTR(FRstrings[fr]);
	Board_UARTPutSTR("\r\n");

	if(fr != FR_OK)
		configASSERT(0);
	int n;
	n = f_puts("prueba escribiendo", &fp);

	Board_UARTPutSTR("f_puts:");
	sprintf(msg, "escribio %d bytes.\r\n",n );
	Board_UARTPutSTR(msg);

	fr = f_close(&fp);
	Board_UARTPutSTR("f_close:");
	Board_UARTPutSTR(FRstrings[fr]);
	Board_UARTPutSTR("\r\n");

	Board_UARTPutSTR("Inicio parpadeo\r\n");



	ledEstados = LED_PARPADEO;
	/* LED1 tsoggle thread */
/*
	xTaskCreate(tIndicadorLed, (signed char *) "Led",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1UL),
				(xTaskHandle *) NULL);

	xTaskCreate(tTransmisionUART, (signed char *) "txUartDb",
						configMINIMAL_STACK_SIZE, (void*) pUartDb, (tskIDLE_PRIORITY + 3UL),
						(xTaskHandle *) NULL);
*/
/*	xTaskCreate(USB_MSCD_Init, (signed char *) "USB_MSCD_Init",
							configMINIMAL_STACK_SIZE, (void*) NULL, (tskIDLE_PRIORITY),
							(xTaskHandle *) &USB_MSCD_Init_handler);*/
	USB_MSCD_Init();
/*
	xTaskCreate(disk_timerproc, (signed char *) "disk_timerproc",
								configMINIMAL_STACK_SIZE, (void*) NULL, (tskIDLE_PRIORITY + 2UL),
								(xTaskHandle *) NULL);
*/

	return 0;
}



/**
 * ----------------------------------------------------------------
 * @fn int32_t AppEstadosCLI(void)
 * @brief aplicacion de prueba de la SD
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t AppEstadosCLI (void) {

	/* inicializo pines de la placa */
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);


	InicializarPinesVOP24v0_Board();

	/* inicializacion de la uart */
	UARTxInicializarEstructura(pUartBT, LPC_UART0);
	UARTxInicializarPines(pUartBT->pUART);
	UARTxInicializar115200bps8N1(pUartBT->pUART);
	UARTxHabilitarIRQ(pUartBT->pUART);

	//Chip_UART_SetBaud(pUartBT->pUART, 1);

	InicializarMDEInterfazTerminal();
	InicializarIndicadorLed();
	ledEstados = LED_PARPADEO;

	/* LED1 tsoggle thread */
	xTaskCreate(tIndicadorLed, (signed char *) "Led",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY),
				(xTaskHandle *) NULL);

	xTaskCreate(tTransmisionUART, (signed char *) "txUartBT",
					configMINIMAL_STACK_SIZE, (void*) pUartBT, (tskIDLE_PRIORITY + 2UL),
					(xTaskHandle *) NULL);

	xTaskCreate(mdeInterfazTerminal, (signed char *) "interfazTerminal",
						configMINIMAL_STACK_SIZE, (void*) pUartBT, (tskIDLE_PRIORITY + 1UL),
						(xTaskHandle *) NULL);


	return 0;
}



/**
 * ----------------------------------------------------------------
 * @fn int32_t AppTestADS1292(void)
 * @brief aplicacion de prueba del ads1292
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t AppTestADS1292 (void) {

	/* inicializo pines de la placa */
	Board_Init();

	/* Initial LED0 state is off */
	Board_LED_Set(0, false);

	/* inicializacion de la uart */
	UARTxInicializarEstructura(pUartBT, LPC_UART0);
	UARTxInicializarPines(pUartBT->pUART);
	UARTxInicializar115200bps8N1(pUartBT->pUART);
	UARTxHabilitarIRQ(pUartBT->pUART);

	InicializarPinesVOP24v0_Board();

	InicializarIndicadorLed();
	ledEstados = LED_PARPADEO;

	/* LED1 tsoggle thread */
	xTaskCreate(tIndicadorLed, (signed char *) "tIndicadorLed",
				configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY),
				(xTaskHandle *) NULL);

	xTaskCreate(tTransmisionUART, (signed char *) "tTransmisionUART",
					configMINIMAL_STACK_SIZE, (void*) pUartBT, (tskIDLE_PRIORITY + 2UL),
					(xTaskHandle *) NULL);

	xTaskCreate(tInicializarADS1292, (signed char *) "tInicializarADS1292",
						configMINIMAL_STACK_SIZE, (void*) pUartBT, (tskIDLE_PRIORITY + 1UL),
						(xTaskHandle *) &tInicializarADS1292_handle);

	return 0;
}
/* -------------------------- implementacion de tareas ------------------------------- */
/**
 *=============================================================================
 * @fn InicializarIndicadorLed
 * @brief inicializacion de la tarea de manejo del led
 * @author nxp
 *=============================================================================
 */

static void tInicializarADS1292 (void* taskParam) {

	uart_t* pUart = (uart_t*) taskParam;

	UARTEscribirString(pUart, "COMIENZO DEL PROGRAMA:\r\n\r\n");
	ADS1292_Inicializar();
	ADS1292_Iniciar_Adquisicion();

	vTaskDelete(tInicializarADS1292_handle);
}


/**
 * ----------------------------------------------------------------
 * @fn static int32_t InicializarIndicadorLed (void)
 * @brief inicializacion de la tarea de manejo del led
 * @author nxp
 * ----------------------------------------------------------------
 */
static int32_t InicializarIndicadorLed (void) {

	Board_Init();
	ledEstados = LED_SIEMPRE_ENCENDIDO;
	return 0;

}

/**
 * ----------------------------------------------------------------
 * @fn static void tIndicadorLed(void *pvParameters)
 * @brief manejo de parpadeo del led
 * @author nxp
 * ----------------------------------------------------------------
 */
static void tIndicadorLed(void *pvParameters) {
	bool estadoLed = false;

	int16_t tiempoEncendidoVar = LED_OSC_TS;
	int16_t tiempoApagadoVar = 0;
	bool sentidoAsc = false;

	while (1) {

		switch(ledEstados){

		case LED_SIEMPRE_ENCENDIDO:
			estadoLed = true;
			Board_LED_Set(0, estadoLed);
			break;

		case LED_SIEMPRE_APAGADO:
			estadoLed = false;
			Board_LED_Set(0, estadoLed);
			break;

		case LED_PARPADEO:
			estadoLed = (bool) !estadoLed;
			Board_LED_Set(0, estadoLed);

			vTaskDelay(LED_PARPADEO_TS);
			break;

		case LED_2PARPADEO_ESPERA:
			estadoLed = true;
			Board_LED_Set(0, estadoLed);
			vTaskDelay(LED_2PARPADEO_ENC_TS);
			estadoLed = false;
			Board_LED_Set(0, estadoLed);
			vTaskDelay(LED_2PARPADEO_ENC_TS);
			estadoLed = true;
			Board_LED_Set(0, estadoLed);
			vTaskDelay(LED_2PARPADEO_ENC_TS);
			estadoLed = false;
			Board_LED_Set(0, estadoLed);
			vTaskDelay(LED_2PARPADEO_ENC_TS);
			vTaskDelay(LED_2PARPADEO_APA_TS);
			break;

		case LED_OSCILAR:

			estadoLed = true;
			Board_LED_Set(0, estadoLed);
			vTaskDelay(tiempoEncendidoVar);

			estadoLed = false;
			Board_LED_Set(0, estadoLed);
			vTaskDelay(tiempoApagadoVar);

			if(!sentidoAsc) {
				tiempoEncendidoVar -= LED_2PARPADEO_PASO;
				tiempoApagadoVar += LED_2PARPADEO_PASO;

				if(tiempoEncendidoVar == 0) {
					//tiempoEncendidoVar = LED_OSC_TS;
					//tiempoApagadoVar = 0;
					sentidoAsc = true;
				}
			}
			else{
				tiempoEncendidoVar += LED_2PARPADEO_PASO;
				tiempoApagadoVar -= LED_2PARPADEO_PASO;

				if(tiempoApagadoVar == 0) {
					//tiempoEncendidoVar = 0;
					// tiempoApagadoVar = LED_OSC_TS;
					sentidoAsc = false;
				}
			}

			break;

		default:
			InicializarIndicadorLed();
		}
	}
}

/**
 * ----------------------------------------------------------------
 * @fn static static void enviarSgnPrueba (void*taskParam)
 * @brief envio señal de prueba mientras el estado este bloqueado
 * @author nxp
 * ----------------------------------------------------------------
 */

static void tEnviarSgnPrueba (void*taskParam) {

	byteWord_t conv;
	uartQueueTipo_t queItem;
	uart_t* pUart = (uart_t*) taskParam;

	static int16_t i;

	i = 0;

	while(1) {

		vTaskDelay(TS_ENVIO_BT);

		conv.word = sgnPrueba[i];

		queItem.buf[0] = conv.bytes[0];
		xQueueSend((pUart->queTx), &queItem, portMAX_DELAY);
		queItem.buf[0] = conv.bytes[1];
		xQueueSend((pUart->queTx), &queItem, portMAX_DELAY);

		i++;
		if(i >= SGN_PRUEBA_N)
			i = 0;

	}


	return;
}

/**
 * ----------------------------------------------------------------
 * @fn static void tEnviarSgnAdquirida (void*taskParam)
 * @brief envio señales adquiridas mientras el estado este bloqueado
 * @author nxp
 * ----------------------------------------------------------------
 */

static void tEnviarSgnAdquirida (void*taskParam) {

	byteWord_t conv;
	uartQueueTipo_t queItem;
	uart_t* pUart = (uart_t*) taskParam;

	int8_t msg[50];

	static int16_t i;

	i = 0;

	UARTEscribirString(pUart, "Inicio SD\r\n");
	vTaskDelay(3000);

	sprintf(msg, "Canales Habilitados:\r\n%d\r\n",canalSel);
	UARTEscribirString(pUart, msg);
	sprintf(msg, "Frecuencia muestreo:\r\n%d\r\n", fsValor);
	UARTEscribirString(pUart, msg);

	while(1) {

		vTaskDelay(TS_ENVIO_BT);

//		conv.word = sgnPrueba[i%128]+rand[i%13]+(2*i);

		queItem.buf[0] = conv.bytes[0];
		xQueueSend((pUart->queTx), &queItem, portMAX_DELAY);
		queItem.buf[0] = conv.bytes[1];
		xQueueSend((pUart->queTx), &queItem, portMAX_DELAY);

		i++;
/*		if(i >= SGN_PRUEBA_N)
			i = 0;
*/
	}


	return;
}


/**
 * ----------------------------------------------------------------
 * @fn void mdeInterfazTerminal (void *pvParameters)
 * @brief interfaz con usuario a traves de la terminal BT
 * @author nxp
 * ----------------------------------------------------------------
 */

int32_t InicializarMDEInterfazTerminal (void) {

	estadoInterfazTerminal = INACTIVO;
	primerIngresoEstadoMDEIntefazTerminal = 1;

	vSemaphoreCreateBinary(semCanalBT);
	if(semCanalBT == NULL) return -1;

	// todo: apagar todos los perifericos
	return 0;
}
/**
 * ----------------------------------------------------------------
 * @fn void mdeInterfazTerminal (void *pvParameters)
 * @brief interfaz con usuario a traves de la terminal BT
 * @author nxp
 * ----------------------------------------------------------------
 */
static void mdeInterfazTerminal (void *taskParam) {

	uartQueueTipo_t queItem;
	uart_t* pUart = (uart_t*) taskParam;
	char msg[20];

	xTaskHandle pTareaInstalada;


#define ASCII_ESC 27
	char clrscr[10];

	sprintf(clrscr, "%c[2J%c[H", ASCII_ESC, ASCII_ESC);

	while(1) {

		switch(estadoInterfazTerminal) {

		case INACTIVO:


			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = 0;
				//UARTEscribirString(pUart, clrscr);
				UARTEscribirString(pUart, "\r\n\r\nVOP24\r\n");
				UARTEscribirString(pUart, "Menu principal\r\n");
				sprintf(msg, "- %c) CONFIGURACION\r\n", CMD_CONFIGURAR);
				UARTEscribirString(pUart, msg);
				sprintf(msg, "- %c) PRUEBA CANAL\r\n", CMD_PROBAR_CANAL);
				UARTEscribirString(pUart, msg);
				sprintf(msg, "- %c) ADQ+ENV\r\n", CMD_ADQ_ENV);
				UARTEscribirString(pUart, msg);
				UARTEscribirString(pUart, "\r\nIngrese opcion:\r\n");
				ledEstados = LED_PARPADEO;
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {
			case CMD_CONFIGURAR:
				InicializarMDEConfiguracion();
				estadoInterfazTerminal = CONFIGURACION;
				primerIngresoEstadoMDEIntefazTerminal = 1;
				break;

			case CMD_PROBAR_CANAL:
				estadoInterfazTerminal = PRUEBA_CANAL;
				primerIngresoEstadoMDEIntefazTerminal = 1;
				break;

			case CMD_ADQ_ENV:
				estadoInterfazTerminal = ADQ_ENV;
				primerIngresoEstadoMDEIntefazTerminal = 1;
				break;

			default:
				estadoInterfazTerminal = INACTIVO;
				UARTEscribirString(pUart, "Comando erroneo\r\n");
			}
			break;

		case CONFIGURACION:
			ledEstados = LED_OSCILAR;
			/* cedo el control del canal de comunicacion */
			xSemaphoreGive(semCanalBT);

			/* instalo tarea mde configuracion con mayor prioridad*/
			xTaskCreate(mdeConfiguracion, (signed char *) "mdeConfiguracion",
					configMINIMAL_STACK_SIZE, (void*) taskParam, (tskIDLE_PRIORITY),
					(xTaskHandle *) &pTareaInstalada);

			/* espero un toque para que pase al frente la tarea de configuracion */
			vTaskDelay(1);

			/* tomo el control del canal de comunicacion */
			xSemaphoreTake(semCanalBT, portMAX_DELAY);

			vTaskDelete(pTareaInstalada);

			estadoInterfazTerminal = INACTIVO;
			primerIngresoEstadoMDEIntefazTerminal = 1;
			UARTEscribirString(pUart, "Saliendo del Modo Configuracion\r\n\r\n");
			break;

		case PRUEBA_CANAL:

			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = 0;
				UARTEscribirString(pUart, "\r\n\r\nTest de canal\r\n");
				sprintf(msg, "- %c) ATRAS\r\n", CMD_VOLVER);
				UARTEscribirString(pUart, msg);

				/* espero a configurar el registro */
				vTaskDelay(2000);

				/* instalo tarea mde para enviar datos con menor prioridad*/
				xTaskCreate(tEnviarSgnPrueba, (signed char *) "enviarSgnPrueba",
						configMINIMAL_STACK_SIZE, (void*) taskParam, (tskIDLE_PRIORITY),
						(xTaskHandle *) &pTareaInstalada);

				ledEstados = LED_PARPADEO;
				// todo: instalar tarea enviar prueba canal
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);


			/* espero a finallizar el registro */
			vTaskDelay(2000);


			switch(queItem.buf[0]) {
			case CMD_VOLVER:
				estadoInterfazTerminal = INACTIVO;
				UARTEscribirString(pUart, "\r\n\r\nSaliendo del Test de Canal\r\n\r\n");
				vTaskDelete(pTareaInstalada);
				// todo: desinstalar tarea enviar prueba canal
				primerIngresoEstadoMDEIntefazTerminal = 1;
				break;

			default:
				estadoInterfazTerminal = PRUEBA_CANAL;
				UARTEscribirString(pUart, "Comando erroneo\r\n");
			}
			break;

		case ADQ_ENV:

			if(primerIngresoEstadoMDEIntefazTerminal) {
				primerIngresoEstadoMDEIntefazTerminal = 0;
				UARTEscribirString(pUart, "\r\n\r\nAdquiero y envio datos\r\n");

				UARTEscribirString(pUart, "Inicializando ADS1292\r\n");
				ADS1292_Inicializar();
				ADS1292_Iniciar_Adquisicion();


				sprintf(msg, "- %c) ADQ+ENV+ALM\r\n", CMD_ADQ_ENV_ALM);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) ATRAS\r\n", CMD_VOLVER);
				UARTEscribirString(pUart, msg);
				ledEstados = LED_2PARPADEO_ESPERA;

				/* instalo tarea para enviar los datos online */
				if(!enviandoOnline) {
					/*xTaskCreate(tEnviarSgnAdquirida, (signed char *) "enviarSgnAdquirida",
										configMINIMAL_STACK_SIZE, (void*) taskParam, (tskIDLE_PRIORITY),
										(xTaskHandle *) &pTareaInstalada);*/
					enviandoOnline = true;
				}
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {
			case CMD_VOLVER:
				estadoInterfazTerminal = INACTIVO;
				UARTEscribirString(pUart, "\r\n\r\n\r\nSaliendo de modo ADQ+ENV\r\n");
				primerIngresoEstadoMDEIntefazTerminal = 1;
				vTaskDelete(pTareaInstalada);
				enviandoOnline = false;
				// desinstalo ADS

				break;

			case CMD_ADQ_ENV_ALM:
				estadoInterfazTerminal = ADQ_ENV_ALM;
				primerIngresoEstadoMDEIntefazTerminal = 1;
				enviandoOnline = true;
				break;

			default:
				estadoInterfazTerminal = ADQ_ENV;
				UARTEscribirString(pUart, "Comando erroneo\r\n");
			}
			break;

		case ADQ_ENV_ALM:

			if(primerIngresoEstadoMDEIntefazTerminal) {
				primerIngresoEstadoMDEIntefazTerminal = 0;
				UARTEscribirString(pUart, "\r\n\r\nModo ADQ+ENV+ALM\r\n");
				UARTEscribirString(pUart, "Abriendo archivo\r\n");

				// todo: instalo tarea almacenar

				sprintf(msg, "- %c) ADQ+ENV\r\n", CMD_ADQ_ENV);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) ADQ+ALM\r\n", CMD_ADQ_ALM);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) ATRAS\r\n", CMD_VOLVER);
				UARTEscribirString(pUart, msg);

				/* instalo tarea para enviar los datos online */
				if(!enviandoOnline) {
					xTaskCreate(tEnviarSgnAdquirida, (signed char *) "enviarSgnAdquirida",
										configMINIMAL_STACK_SIZE, (void*) taskParam, (tskIDLE_PRIORITY),
										(xTaskHandle *) &pTareaInstalada);
					enviandoOnline = true;
				}
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {
			case CMD_VOLVER:
				estadoInterfazTerminal = INACTIVO;
				UARTEscribirString(pUart, "\r\n\r\n\r\nCerrando archivo\r\n");
				UARTEscribirString(pUart, "Desactivo ADS1292\r\n");
				UARTEscribirString(pUart, "Saliendo de modo ADQ+ENV+ALM\r\n");
				primerIngresoEstadoMDEIntefazTerminal = 1;
				vTaskDelete(pTareaInstalada);
				enviandoOnline = false;
				// desinstalo ADS
				// cierro archivo
				// cierro SD
				break;

			case CMD_ADQ_ENV:
				estadoInterfazTerminal = ADQ_ENV;
				primerIngresoEstadoMDEIntefazTerminal = 1;
				UARTEscribirString(pUart, "\r\n\r\n\r\nCerrando archivo\r\n");
				break;

			case CMD_ADQ_ALM:
				estadoInterfazTerminal = ADQ_ALM;
				UARTEscribirString(pUart, "\r\n\r\n\r\nFinaliza envio de sgn.\r\n");
				UARTEscribirString(pUart, "Se continua adquiriendo y almacenando.\r\n");
				primerIngresoEstadoMDEIntefazTerminal = 1;
				vTaskDelete(pTareaInstalada);
				enviandoOnline = false;
				break;

			default:
				estadoInterfazTerminal = ADQ_ENV_ALM;
				UARTEscribirString(pUart, "\r\n\r\nComando erroneo\r\n");
			}

			break;

		case ADQ_ALM:

			if(primerIngresoEstadoMDEIntefazTerminal) {
				primerIngresoEstadoMDEIntefazTerminal = 0;
				UARTEscribirString(pUart, "\r\n\r\n\r\nModo ADQ+ALM\r\n");

				sprintf(msg, "- %c) ADQ+ENV+ALM\r\n", CMD_ADQ_ENV_ALM);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) ATRAS\r\n", CMD_VOLVER);
				UARTEscribirString(pUart, msg);

			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {
			case CMD_VOLVER:
				estadoInterfazTerminal = INACTIVO;
				UARTEscribirString(pUart, (uint8_t*)"\r\n\r\n\r\nCerrando archivo\r\n");
				UARTEscribirString(pUart, (uint8_t*)"Desactivo ADS1292\r\n");
				UARTEscribirString(pUart, (uint8_t*)"Saliendo de modo ADQ+ALM\r\n");
				primerIngresoEstadoMDEIntefazTerminal = 1;
				// desinstalo ADS
				break;

			case CMD_ADQ_ENV_ALM:
				estadoInterfazTerminal = ADQ_ENV_ALM;
				primerIngresoEstadoMDEIntefazTerminal = 1;
				break;


			default:
				estadoInterfazTerminal = ADQ_ALM;
				UARTEscribirString(pUart, "\r\n\r\nComando erroneo\r\n");
			}

			break;

		default:
			InicializarMDEInterfazTerminal();

		}
	}
}

estadoConfiguracion_t estadoConfiguracion;

/**
 * ----------------------------------------------------------------
 * @fn void mdeInterfazTerminal (void *pvParameters)
 * @brief interfaz con usuario a traves de la terminal BT
 * @author nxp
 * ----------------------------------------------------------------
 */

int32_t InicializarMDEConfiguracion (void) {
	estadoConfiguracion = CONF_INICIO;
	primerEntradaEstadoMDEConfiguracion = true;
	calP0 = 0;
	calP1 = 0;
	return 0;
}

/**
 * ----------------------------------------------------------------
 * @fn void mdeInterfazTerminal (void *pvParameters)
 * @brief interfaz con usuario a traves de la terminal BT
 * @author nxp
 * ----------------------------------------------------------------
 */
#define SEPARADOR_FECHA '/'
#define SEPARADOR_HORA  ':'

static void mdeConfiguracion (void* taskParam) {

	uartQueueTipo_t queItem;
	uint8_t msg[50];

	uint8_t pgaCanalSel = 0;
	uint8_t calCanalSel = 0;
	uart_t* pUart = (uart_t*) taskParam;

	int8_t diaPrimerCifra;
	int8_t diaSegundaCifra;
	int8_t diaIngresado;

	int8_t mesPrimerCifra;
	int8_t mesSegundaCifra;
	int8_t mesIngresado;

	int8_t anioPrimerCifra;
	int8_t anioSegundaCifra;
	int8_t anioTercerCifra;
	int8_t anioCuartaCifra;
	int16_t anioIngresado;

	int8_t barra1, barra2;

	int8_t horaPrimerCifra;
	int8_t horaSegundaCifra;
	int8_t horaIngresada;

	int8_t minPrimerCifra;
	int8_t minSegundaCifra;
	int8_t minIngresado;

	int8_t segPrimerCifra;
	int8_t segSegundaCifra;
	int8_t segIngresado;

	int8_t sepHora1, sepHora2;

	/* tomo el control del canal bt para bloquear mde de mayor prioridad  */
	xSemaphoreTake(semCanalBT, portMAX_DELAY);

	while(1) {

		switch(estadoConfiguracion) {
		case CONF_INICIO:

			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nModo Configuracion\r\n");

				sprintf(msg, "- %c) COMPROBAR HARDWARE\r\n", CMD_COMPROBAR_HW);
				UARTEscribirString(pUart, msg);
				sprintf(msg, "- %c) ACTIVAR/DESACTIVAR CANALES\r\n", CMD_ACTIVAR_CANALES);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) CONFIGURAR GANANCIA\r\n", CMD_PGA_SEL_CANAL);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) MODIFICAR FS\r\n", CMD_MODIFICAR_FS);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) MODIFICAR FECHA\r\n", CMD_MODIFICAR_FECHA);
				UARTEscribirString(pUart, msg);
				sprintf(msg, "- %c) MODIFICAR HORA\r\n", CMD_MODIFICAR_HORA);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) CALIBRAR CANAL\r\n", CMD_CAL_SEL_CANAL);
				UARTEscribirString(pUart, msg);

				sprintf(msg, "- %c) ATRAS\r\n", CMD_VOLVER);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {
			case CMD_COMPROBAR_HW:
				estadoConfiguracion = COMPROBAR_HW;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_ACTIVAR_CANALES:
				estadoConfiguracion = ACTIVAR_CANALES;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_PGA_SEL_CANAL:
				estadoConfiguracion = PGA_SEL_CANAL;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_MODIFICAR_FS:
				estadoConfiguracion = MODIFICAR_FS;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_MODIFICAR_FECHA:
				estadoConfiguracion = MODIFICAR_FECHA;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_MODIFICAR_HORA:
				estadoConfiguracion = MODIFICAR_HORA;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_CAL_SEL_CANAL:
				estadoConfiguracion = CAL_SEL_CANAL;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_VOLVER:
				xSemaphoreGive(semCanalBT);
				break;

			default:
				estadoConfiguracion = CONF_INICIO;
			}

			break;

		case COMPROBAR_HW:

			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart,(uint8_t*) "\r\nComprobacion de hardware\r\n");
				UARTEscribirString(pUart,(uint8_t*) "   Comprobando memoria SD...\r\n");
				vTaskDelay(2000);
				UARTEscribirString(pUart,(uint8_t*) "   Memoria SD OK...\r\n");
				UARTEscribirString(pUart,(uint8_t*) "   Comprobando ADS1292...\r\n");
				vTaskDelay(500);
				UARTEscribirString(pUart,(uint8_t*) "   ADS1292 OK...\r\n");
				UARTEscribirString(pUart,(uint8_t*) "   Comprobacion finalizada\r\n");
			}
			estadoConfiguracion = CONF_INICIO;
			primerIngresoEstadoMDEIntefazTerminal = true;
			break;

		case ACTIVAR_CANALES:

			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart,(uint8_t*) "\r\nSeleccione canal a activar\r\n");
				UARTEscribirString(pUart,(uint8_t*) "1) Canal 1\r\n");
				UARTEscribirString(pUart,(uint8_t*) "2) Canal 2\r\n");
				UARTEscribirString(pUart,(uint8_t*) "3) Canal 1 + 2\r\n");
				sprintf((char*)msg, "%c) ATRAS\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {

			case '1':
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				vTaskDelay(500);
				UARTEscribirString(pUart, (uint8_t*)"\r\nCanal 1 activo\r\n");
				canalSel = 1;
				break;

			case '2':
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				vTaskDelay(500);
				UARTEscribirString(pUart,(uint8_t*) "\r\nCanal 2 activo\r\n");
				canalSel = 2;
				break;

			case '3':
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				vTaskDelay(500);
				UARTEscribirString(pUart,(uint8_t*) "\r\nCanal 1+2 activos\r\n");
				canalSel = 3;
				break;

			case CMD_CANCELAR:
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			default:
				UARTEscribirString(pUart, (uint8_t*)"Comando erroneo\r\n");

			}
			break;

		case PGA_SEL_CANAL:

			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart,(uint8_t*) "\r\nSeleccione el canal a configurar\r\n");
				UARTEscribirString(pUart,(uint8_t*) "1) Canal 1\r\n");
				UARTEscribirString(pUart,(uint8_t*) "2) Canal 2\r\n");
				sprintf((char*)msg, "%c) ATRAS\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {

			case '1':
				UARTEscribirString(pUart, "Canal 1 seleccionado\r\n");
				estadoConfiguracion = PGA_VALOR;
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanalSel = 1;
				break;

			case '2':
				UARTEscribirString(pUart, "Canal 2 seleccionado\r\n");
				estadoConfiguracion = PGA_VALOR;
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanalSel = 2;
				break;

			case CMD_CANCELAR:
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			default:
				UARTEscribirString(pUart, "Comando erroneo\r\n");

			}
			break;

		case PGA_VALOR:

			if(primerIngresoEstadoMDEIntefazTerminal) {
				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nSeleccione la ganancia PGA:\r\n");
				UARTEscribirString(pUart, "0) PGA = 6 (default)\r\n");
				UARTEscribirString(pUart, "1) PGA = 1\r\n");
				UARTEscribirString(pUart, "2) PGA = 2\r\n");
				UARTEscribirString(pUart, "3) PGA = 3\r\n");
				UARTEscribirString(pUart, "4) PGA = 4\r\n");
				UARTEscribirString(pUart, "5) PGA = 8\r\n");
				UARTEscribirString(pUart, "6) PGA = 12\r\n");
				sprintf(msg, "%c) ATRAS\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {

			case '0':

				sprintf(msg, "Canal %d - PGA = 6\r\n", pgaCanalSel);
				UARTEscribirString(pUart, msg);
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanal[pgaCanalSel - 1] = 6;
				estadoConfiguracion = CONF_INICIO;
				break;

			case '1':
				sprintf(msg, "Canal %d - PGA = 1\r\n", pgaCanalSel);
				UARTEscribirString(pUart, msg);
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanal[pgaCanalSel - 1] = 1;
				estadoConfiguracion = CONF_INICIO;
				break;
			case '2':
				sprintf(msg, "Canal %d - PGA = 2\r\n", pgaCanalSel);
				UARTEscribirString(pUart, msg);
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanal[pgaCanalSel - 1] = 2;
				estadoConfiguracion = CONF_INICIO;
				break;
			case '3':
				sprintf(msg, "Canal %d - PGA = 3\r\n", pgaCanalSel);
				UARTEscribirString(pUart, msg);
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanal[pgaCanalSel - 1] = 3;
				estadoConfiguracion = CONF_INICIO;
				break;
			case '4':
				sprintf(msg, "Canal %d - PGA = 4\r\n", pgaCanalSel);
				UARTEscribirString(pUart, msg);
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanal[pgaCanalSel - 1] = 4;
				estadoConfiguracion = CONF_INICIO;
				break;
			case '5':
				sprintf(msg, "Canal %d - PGA = 8\r\n", pgaCanalSel);
				UARTEscribirString(pUart, msg);
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanal[pgaCanalSel - 1] = 8;
				estadoConfiguracion = CONF_INICIO;
				break;
			case '6':
				sprintf(msg, "Canal %d - PGA = 12\r\n", pgaCanalSel);
				UARTEscribirString(pUart, msg);
				primerIngresoEstadoMDEIntefazTerminal = true;
				pgaCanal[pgaCanalSel - 1] = 12;
				estadoConfiguracion = CONF_INICIO;
				break;

			case CMD_CANCELAR:
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			default:
				UARTEscribirString(pUart, "Comando erroneo\r\n");

			}
			break;

		case MODIFICAR_FS:

			if(primerIngresoEstadoMDEIntefazTerminal) {
				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nSeleccione la FS:\r\n");
				UARTEscribirString(pUart, "1) 125 SPS\r\n");
				UARTEscribirString(pUart, "2) 250 SPS\r\n");
				UARTEscribirString(pUart, "3) 500 SPS (default)\r\n");
				UARTEscribirString(pUart, "4) 1 kSPS\r\n");
				sprintf(msg, "%c) ATRAS\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {

			case '1':
				fsSel = ADS_CONFIG_1_OS_RATIO_125SPS;
				fsValor = 125;
				UARTEscribirString(pUart, "FS seleccionada\r\n");
				primerIngresoEstadoMDEIntefazTerminal = true;
				estadoConfiguracion = CONF_INICIO;
				break;

			case '2':
				fsSel = ADS_CONFIG_1_OS_RATIO_250SPS;
				fsValor = 250;
				UARTEscribirString(pUart, "FS seleccionada\r\n");
				primerIngresoEstadoMDEIntefazTerminal = true;
				estadoConfiguracion = CONF_INICIO;
				break;

			case '3':
				fsSel = ADS_CONFIG_1_OS_RATIO_500SPS;
				fsValor = 500;
				UARTEscribirString(pUart, "FS seleccionada\r\n");
				primerIngresoEstadoMDEIntefazTerminal = true;
				estadoConfiguracion = CONF_INICIO;
				break;

			case '4':
				fsSel = ADS_CONFIG_1_OS_RATIO_1kSPS;
				fsValor = 1000;
				UARTEscribirString(pUart, "FS seleccionada\r\n");
				primerIngresoEstadoMDEIntefazTerminal = true;
				estadoConfiguracion = CONF_INICIO;
				break;

			case CMD_CANCELAR:
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			default:
				UARTEscribirString(pUart, "Comando erroneo\r\n");

			}
			break;


		case MODIFICAR_FECHA:

			if(primerIngresoEstadoMDEIntefazTerminal) {
				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nIngrese fecha en formato <<dd/mm/aaaa>> :\r\n");
				sprintf(msg, "(Ingrese '%c' para salir)\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			diaPrimerCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			diaSegundaCifra = CifraAsciiEntero(queItem.buf[0]);

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			barra1 = queItem.buf[0];

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			mesPrimerCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			mesSegundaCifra = CifraAsciiEntero(queItem.buf[0]);

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			barra2 = queItem.buf[0];

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			anioPrimerCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			anioSegundaCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			anioTercerCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			anioCuartaCifra = CifraAsciiEntero(queItem.buf[0]);


			if(((barra1 != SEPARADOR_FECHA) || (barra2 != SEPARADOR_FECHA)) ||
			   ((diaPrimerCifra == -1) || (diaSegundaCifra == -1)) ||
			   ((mesPrimerCifra == -1) || (mesSegundaCifra == -1)) ||
			   ((anioPrimerCifra == -1) || (anioSegundaCifra == -1) || (anioTercerCifra == -1) || (anioCuartaCifra == -1))) {

				UARTEscribirString(pUart, "Formato erroneo\r\n");
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
			}
			else {
				diaIngresado = diaPrimerCifra*10 + diaSegundaCifra;
				mesIngresado = mesPrimerCifra*10 + mesSegundaCifra;
				anioIngresado = anioPrimerCifra*1000 + anioSegundaCifra*100 + anioTercerCifra*10 + anioCuartaCifra;
				UARTEscribirString(pUart, "Fecha configurada correctamente\r\n");
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
			}
			break;

		case MODIFICAR_HORA:

			if(primerIngresoEstadoMDEIntefazTerminal) {
				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nIngrese hora en formato <<hh:mm:ss>> :\r\n");
				sprintf(msg, "(Ingrese '%c' para salir)\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			horaPrimerCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			horaSegundaCifra = CifraAsciiEntero(queItem.buf[0]);

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			sepHora1 = queItem.buf[0];

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			minPrimerCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			minSegundaCifra = CifraAsciiEntero(queItem.buf[0]);

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			sepHora2 = queItem.buf[0];

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			segPrimerCifra = CifraAsciiEntero(queItem.buf[0]);
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			segSegundaCifra = CifraAsciiEntero(queItem.buf[0]);


			if(((sepHora1 != SEPARADOR_HORA) || (sepHora2 != SEPARADOR_HORA)) ||
			   ((horaPrimerCifra == -1) || (horaSegundaCifra == -1)) ||
			   ((minPrimerCifra == -1) || (minSegundaCifra == -1)) ||
			   ((segPrimerCifra == -1) || (segSegundaCifra == -1))) {

				UARTEscribirString(pUart, "Formato erroneo\r\n");
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
			}
			else {
				horaIngresada = diaPrimerCifra*10 + diaSegundaCifra;
				minIngresado = minPrimerCifra*10 + minSegundaCifra;
				segIngresado = segPrimerCifra*10 + segSegundaCifra;
				UARTEscribirString(pUart, "Hora configurada correctamente\r\n");
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
			}
			break;

		case CAL_SEL_CANAL:

			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nSeleccione el canal a calibrar\r\n");
				UARTEscribirString(pUart, "1) Canal 1\r\n");
				UARTEscribirString(pUart, "2) Canal 2\r\n");
				sprintf(msg, "%c) ATRAS\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}

			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);
			switch(queItem.buf[0]) {

			case '1':
				UARTEscribirString(pUart, "Canal 1 seleccionado\r\n");
				estadoConfiguracion = CAL_P0;
				primerIngresoEstadoMDEIntefazTerminal = true;
				calCanalSel = 1;
				break;

			case '2':
				UARTEscribirString(pUart, "Canal 2 seleccionado\r\n");
				estadoConfiguracion = CAL_P0;
				primerIngresoEstadoMDEIntefazTerminal = true;
				calCanalSel = 2;
				break;

			case CMD_CANCELAR:
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			default:
				UARTEscribirString(pUart, "Comando erroneo\r\n");
			}
			break;

		case CAL_P0:

			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nCalibracion 0mV:\r\n");
				sprintf(msg, "%c) Tomar Medicion\r\n", CMD_CAL_P0);
				UARTEscribirString(pUart, msg);
				sprintf(msg, "%c) Cancelar\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);

			switch(queItem.buf[0]) {

			case CMD_CAL_P0:
				calP0 = 0x1234; // primera medicion
				estadoConfiguracion = CAL_P1;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_CANCELAR:
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			default:
				UARTEscribirString(pUart, "Comando erroneo\r\n");
			}

			break;

		case CAL_P1:
			if(primerIngresoEstadoMDEIntefazTerminal) {

				primerIngresoEstadoMDEIntefazTerminal = false;
				UARTEscribirString(pUart, "\r\nCalibracion 100mV:\r\n");
				sprintf(msg, "%c) Tomar Medicion\r\n", CMD_CAL_P1);
				UARTEscribirString(pUart, msg);
				sprintf(msg, "%c) Cancelar\r\n", CMD_CANCELAR);
				UARTEscribirString(pUart, msg);
			}
			/* detengo la funcion esperando un dato de la uart elegida */
			xQueueReceive(pUart->queRx, &queItem, portMAX_DELAY);

			switch(queItem.buf[0]) {
			case CMD_CAL_P1:
				calP0 = 0x2345; // segunda medicion
				estadoConfiguracion = CONF_INICIO;
				UARTEscribirString(pUart, "\r\nCalibracion guardada\r\n");
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			case CMD_CANCELAR:
				estadoConfiguracion = CONF_INICIO;
				primerIngresoEstadoMDEIntefazTerminal = true;
				break;

			default:
				UARTEscribirString(pUart, "Comando erroneo\r\n");
			}
			break;

		default:
			UARTEscribirString(pUart, "Comando erroneo\r\n");

		}
	}

}

/**
 * ----------------------------------------------------------------
 * @fn int8_t CifraAsciiEntero (int8_t cifraAscii)
 * @brief convierte cifra en ascii a entero. retorna -1 si hay error
 * @author nxp
 * ----------------------------------------------------------------
 */
int8_t CifraAsciiEntero (int8_t cifraAscii) {

	if((cifraAscii >= '0') && (cifraAscii <= '9')) {
		return (cifraAscii - '0');
	}
	else
		return -1;
}
