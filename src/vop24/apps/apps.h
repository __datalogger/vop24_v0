/*
 * apps.h
 *
 *  Created on: 8 ago. 2019
 *      Author: froux
 */

#ifndef SRC_APPS_APPS_H_
#define SRC_APPS_APPS_H_

#include <sapi_sdcard.h>

/* ----------------------- define -------------------------- */

#define	ADS_CONFIG_1_OS_RATIO_MAS				(0x07 << 0)					// These bits determine the oversampling ratio of both channel 1 - 2
	#define	ADS_CONFIG_1_OS_RATIO_125SPS		(0x00 << 0)					// fMOD / 1024 - 125SPS
	#define	ADS_CONFIG_1_OS_RATIO_250SPS		(0x01 << 0)					// fMOD / 512 - 250SPS
	#define	ADS_CONFIG_1_OS_RATIO_500SPS		(0x02 << 0)					// fMOD / 256 - 500SPS
	#define	ADS_CONFIG_1_OS_RATIO_1kSPS			(0x03 << 0)					// fMOD / 128 - 1kSPS
	#define	ADS_CONFIG_1_OS_RATIO_2kSPS			(0x04 << 0)					// fMOD / 64 - 2kSPS
	#define	ADS_CONFIG_1_OS_RATIO_4kSPS			(0x05 << 0)					// fMOD / 32 - 4kSPS
	#define	ADS_CONFIG_1_OS_RATIO_8kSPS			(0x06 << 0)					// fMOD / 16 - 8kSPS


/* manejo del led indicador */
#define LED_PARPADEO_TS				500
#define LED_2PARPADEO_ENC_TS		250
#define LED_2PARPADEO_APA_TS		1000
#define LED_2PARPADEO_PASO			1

#define LED_OSC_TS					20

/** @brief periodo de muestreo para envio online */
#define TS_ENVIO_BT	10
/* ----------------------- tipos de dato --------------------------- */

/** @brief estados de la maquina de estados interfaz terminal */
typedef enum {INACTIVO, CONFIGURACION, PRUEBA_CANAL, ADQ_ENV, ADQ_ENV_ALM, ADQ_ALM} estadoInterfazTerminal_t;

/** @brief estados de la mde de configuracion */
typedef enum {CONF_INICIO, COMPROBAR_HW, ACTIVAR_CANALES, PGA_SEL_CANAL, PGA_VALOR, MODIFICAR_FS, MODIFICAR_FECHA, MODIFICAR_HORA, CAL_SEL_CANAL, CAL_P0, CAL_P1} estadoConfiguracion_t;

typedef enum {CMD_COMPROBAR_HW = 'a', CMD_ACTIVAR_CANALES, CMD_PGA_SEL_CANAL, CMD_PGA_VALOR, CMD_MODIFICAR_FS,
			  CMD_MODIFICAR_FECHA, CMD_MODIFICAR_HORA, CMD_CAL_SEL_CANAL, CMD_CAL_P0, CMD_CAL_P1, CMD_CANCELAR = 'y'} comandosConfiguracion_t;



/** @brief comandos de la mde Interfaz Terminal*/
typedef enum {CMD_CONFIGURAR = 'a', CMD_PROBAR_CANAL, CMD_ADQ_ENV, CMD_ADQ_ENV_ALM, CMD_ADQ_ALM, CMD_VOLVER = 'x'} comandosInterfazTerminal_t;

/** @brief estados del manejo de led */
typedef enum {LED_SIEMPRE_ENCENDIDO, LED_SIEMPRE_APAGADO, LED_PARPADEO, LED_2PARPADEO_ESPERA, LED_OSCILAR} ledEstados_t;

/* ----------------------- aplicaciones --------------------------- */

extern int32_t AppViaUartBLE_MLTBT05 (void);
extern int32_t AppLoopUarts (void);
extern int32_t AppTestSD (void);
extern int32_t AppTest_USBMSCD_SD (void);
extern int32_t AppEstadosCLI (void);
extern int32_t AppTestADS1292 (void);

extern sdcard_t sdcard;
extern xTaskHandle USB_MSCD_Init_handler;
extern bool usbInicializado;

int8_t CifraAsciiEntero (int8_t cifraAscii);
/* ----------------------- vars. globales --------------------------- */

extern bool enviandoOnline;


#endif /* SRC_APPS_APPS_H_ */
