/*
 * uart.h
 *
 *  Created on: 6 ago. 2019
 *      Author: froux
 */

#ifndef SRC_UART_UART_H_
#define SRC_UART_UART_H_


/* pines de la uart 0 */
#define LPC_UART0_TXD_PORT			0
#define LPC_UART0_TXD_PIN			2
#define LPC_UART0_TXD_IOCON_FUNC 	IOCON_FUNC1
#define LPC_UART0_RXD_PORT			0
#define LPC_UART0_RXD_PIN			3
#define LPC_UART0_RXD_IOCON_FUNC	IOCON_FUNC1
/* pines de la uart 1 */
#define LPC_UART3_TXD_PORT		0
#define LPC_UART3_TXD_PIN		0
#define LPC_UART3_TXD_IOCON_FUNC	IOCON_FUNC2
#define LPC_UART3_RXD_PORT		0
#define LPC_UART3_RXD_PIN		1
#define LPC_UART3_RXD_IOCON_FUNC	IOCON_FUNC2


/** @brief largo de las queue de Rx y Tx */
#define QUEUE_UART_L                        ((unsigned portBASE_TYPE) 32)

/** @brief largo del tipo de dato de la cola de datos */
#define QUE_UART_BUF_L	 					1

/** @brief cantidad de bits de las FIFO de la uart */
#define UART_FIFO_L                         16

/* ------------------------------------------------- tipos de dato -------------------------------------- */

/** @brief  tipo de dato para el manejo general de uart. */
typedef struct {
  LPC_USART_T* pUART;				/* puntero a estructura de la uart */
  xQueueHandle queRx;				/* cola de datos de recepcion */
  xQueueHandle queTx;				/* cola de datos de transmision */
  xSemaphoreHandle semTx;			/* semaforo para controlar la salida de datos */

  /* agregar callback al handler*/
}uart_t;

/** @brief tipo de dato de las queue de la uart */
typedef struct {
	char buf[QUE_UART_BUF_L];
}uartQueueTipo_t;

/** @brief tipo de dato para pasar de word a bytes */
typedef union{
	uint8_t bytes[2];
	uint16_t word;
}byteWord_t;




/* - ----------------------- tareas --------------------------- */
extern void tPasamanos (void *taskParam);
extern void tTransmisionUART (void* taskParam );
extern void tLoopUART (void *pvParameters);

/* ------------------------------------ funciones externas -------------------------------------- */
extern int32_t UARTxInicializarEstructura (uart_t*uart, LPC_USART_T* pUART);
extern int32_t UARTxInicializarPines (LPC_USART_T* pUART);
extern int32_t UARTxInicializar115200bps8N1(LPC_USART_T* pUART);
extern int32_t UARTxHabilitarIRQ(LPC_USART_T* pUART);

extern int32_t UARTEscribirString (uart_t*pUart, uint8_t*buf);
/* -------------------------------- variables globales  --------------------------- */

extern uart_t* pUartBT;
extern uart_t* pUartDb;

extern uart_t* viaUart1[2];
extern uart_t* viaUart2[2];

#endif /* SRC_UART_UART_H_ */
