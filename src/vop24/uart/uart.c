/*
 * uart.c
 *
 *  Created on: 6 ago. 2019
 *      Author: froux
 */
/* -------------------------------- inclusión de archivos --------------------- */

#include <stdint.h>
#include "chip.h"
#include "board.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "uart.h"

/* - ----------------------- tareas --------------------------- */
void tPasamanos (void *taskParam);
void tTransmisionUART (void* taskParam );
void tLoopUART (void *taskParam);

/* - ----------------------- funciones globales  --------------------------- */
int32_t UARTxInicializarEstructura (uart_t*uart, LPC_USART_T* pUART);
int32_t UARTxInicializarPines (LPC_USART_T* pUART);
int32_t UARTxInicializar115200bps8N1(LPC_USART_T* pUART);
int32_t UARTxHabilitarIRQ(LPC_USART_T* pUART);
int32_t UARTEscribirString (uart_t*pUart, uint8_t*buf);
/* -------------------------------- variables globales  --------------------------- */

uart_t uart0;
uart_t uart3;

uart_t* pUartBT = &uart0;
uart_t* pUartDb = &uart3;

uart_t* viaUart1[2] = {&uart0, &uart3};
uart_t* viaUart2[2] = {&uart3, &uart0};

/* -------------------------------- implementación de tareas  --------------------- */

/**
 * ----------------------------------------------------------------
 * @fn int32_t tPasamanos (void *pvParameters)
 * @brief tarea que pasa de una uart a la otra
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */
void tPasamanos (void *taskParam) {

	uartQueueTipo_t queItem;
	uart_t* pUartRx = ((uart_t**) taskParam) [0];
	uart_t* pUartTx = ((uart_t**) taskParam) [1];

	while(1) {
		xQueueReceive((pUartRx->queRx), &queItem, portMAX_DELAY);
		xQueueSend((pUartTx->queTx), &queItem, portMAX_DELAY);
	}
}

/**
 * ----------------------------------------------------------------
 * @fn int32_t tLoopUART (void *pvParameters)
 * @brief tarea que transmite lo que recibe
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */
void tLoopUART (void *taskParam) {

	uartQueueTipo_t queItem;
	uart_t* pUart = (uart_t*) taskParam;

	while(1) {
		xQueueReceive((pUart->queRx), &queItem, portMAX_DELAY);
		xQueueSend((pUart->queTx), &queItem, portMAX_DELAY);
	}
}

/**
 * ----------------------------------------------------------------
 * @fn int32_t tTransmisionUART (void)
 * @brief tarea que saca los bits de una cola de datos y los escribe
 * @brief usando la fifo de salida (corto de a 16 bytes)
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

void tTransmisionUART (void* taskParam ) {

  uartQueueTipo_t token;
  uint8_t j;
  int32_t i = 0;

  uart_t* pUart = (uart_t*) taskParam;


  while(1) {

	/* intento tomar un dato de la cola de transmision */
    xQueueReceive(pUart->queTx, &token, portMAX_DELAY);

    /* reinicio contador */
    i = 0;

    /* intento tomar semaforo de thre */
    xSemaphoreTake(pUart->semTx, portMAX_DELAY);

    /* paso a vaciar el buffer sobre la fifo */
    for(j = 0; j < QUE_UART_BUF_L; j++) {
     /* recorro los bits */
     if((i + 1) >=  UART_FIFO_L){
    	 /* intento tomar semaforo de thre */
		 xSemaphoreTake(pUart->semTx, portMAX_DELAY);
		 /* reinicio contador */
         i = 0;
     }

     Chip_UART_SendByte(pUart->pUART, token.buf[j]);
     i++;
    }
  }
}


/* -------------------------------- implementación de funciones  --------------------- */



/**
 * ----------------------------------------------------------------
 * @fn int32_t UARTEscribirString (uint8_t*buf)
 * @brief escribe un string en la cola de salida
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */
int32_t UARTEscribirString (uart_t*pUart, uint8_t*buf) {

	uartQueueTipo_t queItem;
	int8_t i = 0;
	int8_t j;

	while(buf[i]!= '\0') {

		for(j = 0;j < QUE_UART_BUF_L;j++, i++)
		{
			if( buf[i] == '\0') return 0;
			queItem.buf[j] = buf[i];
		}

		xQueueSend((pUart->queTx), &queItem, portMAX_DELAY);

	}

	return 0;
}

/**
 * ----------------------------------------------------------------
 * @fn int32_t UARTxInicializarEstructura (void)
 * @brief inicializa los campos de la estructura
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t UARTxInicializarEstructura (uart_t*uart, LPC_USART_T* pUART) {

	uart->pUART = pUART;

	uart->queRx = xQueueCreate(QUEUE_UART_L, sizeof(uartQueueTipo_t));
	if(uart->queRx == NULL) return -1;

	uart->queTx = xQueueCreate(QUEUE_UART_L, sizeof(uartQueueTipo_t));
	if(uart->queTx == NULL) return -1;

	vSemaphoreCreateBinary(uart->semTx);
	if(uart->semTx == NULL) return -1;

	return 0;
}


/**
 * ----------------------------------------------------------------
 * @fn int32_t UARTxInicializarPines (void)
 * @brief inicializa los pines de la UARTx
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t UARTxInicializarPines (LPC_USART_T* pUART) {

	if( pUART == LPC_UART0) {
		Chip_IOCON_PinMux(LPC_IOCON, LPC_UART0_TXD_PORT, LPC_UART0_TXD_PIN,IOCON_MODE_INACT, LPC_UART0_TXD_IOCON_FUNC);
		Chip_IOCON_PinMux(LPC_IOCON, LPC_UART0_RXD_PORT, LPC_UART0_RXD_PIN,IOCON_MODE_INACT, LPC_UART0_RXD_IOCON_FUNC); }
	else if( pUART == LPC_UART3) {
		Chip_IOCON_PinMux(LPC_IOCON, LPC_UART3_TXD_PORT, LPC_UART3_TXD_PIN,IOCON_MODE_INACT, LPC_UART3_TXD_IOCON_FUNC);
		Chip_IOCON_PinMux(LPC_IOCON, LPC_UART3_RXD_PORT, LPC_UART3_RXD_PIN,IOCON_MODE_INACT, LPC_UART3_RXD_IOCON_FUNC);
	}
	return 0;
}
/**
 * ----------------------------------------------------------------
 * @fn int32_t UARTxInicializar115200bps8N1(LPC_USART_T* pUART)
 * @brief inicializa una UART por default en 115200bps 8N1
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t UARTxInicializar115200bps8N1(LPC_USART_T* pUART) {

	/* Setup UART for 115.2K8N1 */
	Chip_UART_Init(pUART);
	Chip_UART_SetBaud(pUART, 115200);
	Chip_UART_ConfigData(pUART, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
	Chip_UART_SetupFIFOS(pUART, (UART_FCR_FIFO_EN | UART_FCR_RX_RS|
								 UART_FCR_TX_RS | UART_FCR_TRG_LEV0));
	Chip_UART_TXEnable(pUART);
	return 0;
}

/**
 * ----------------------------------------------------------------
 * @fn int32_t UARTxHabilitarIRQ(LPC_USART_T* pUART)
 * @brief habilito la IRQ de la UART
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t UARTxHabilitarIRQ(LPC_USART_T* pUART) {


	/* Enable receive data and line status interrupt */
	Chip_UART_IntEnable(pUART, (UART_IER_RBRINT | UART_IER_THREINT | UART_IER_RLSINT |
								UART_IER_RLSINT));

	/* preemption = 1, sub-priority = 1 */
	// NVIC_SetPriority(IRQ_SELECTION, 1);
	if( pUART == LPC_UART0)
		NVIC_EnableIRQ(UART0_IRQn);
	else if( pUART == LPC_UART3)
		NVIC_EnableIRQ(UART3_IRQn);
	return 0;
}

/* ----------------------- handlers de irq ---------------------- */

/**
 * ----------------------------------------------------------------
 * @fn int32_t UART0_IRQHandler(LPC_USART_T* pUART)
 * @brief handler de la IRQ de la uart 0
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

void UART0_IRQHandler (void) {

   signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

   uartQueueTipo_t itemQueue;

   /* levanto el registro de causa de interrupcion */
   uint32_t regIIR = Chip_UART_ReadIntIDReg(uart0.pUART);
   /* levanto RLS */
   uint32_t status = Chip_UART_ReadLineStatus(uart0.pUART);
   uint32_t intId;

   /* pregunto si tengo una IRQ pendiente */
   if(regIIR & UART_IIR_INTSTAT_PEND)
	   return;

   /* copio la causa de irq */
   intId = regIIR & UART_IIR_INTID_MASK;

   switch(intId) {

   case UART_IIR_INTID_RLS:      /*!< Interrupt identification: Receive line interrupt */
	   break;

   case UART_IIR_INTID_RDA:      /*!< Interrupt identification: Receive data available interrupt */

	   if(status & UART_LSR_RDR) { // uartRxReady

		   itemQueue.buf[0] = Chip_UART_ReadByte(uart0.pUART);
		   xQueueSendFromISR(uart0.queRx, &itemQueue, &xHigherPriorityTaskWoken);
	   }
	   else {
		   /* si llego aca es porque hubo algun error en la trama */
	   }
	   break;

	case UART_IIR_INTID_CTI:      /*!< Interrupt identification: Character time-out indicator interrupt */
		// reinicio fifo de recepcion
		Chip_UART_SetupFIFOS(uart0.pUART, UART_FCR_RX_RS);
		break;

	case UART_IIR_INTID_THRE:     /*!< Interrupt identification: THRE interrupt */

		if( (status & UART_LSR_THRE) || (status & UART_LSR_TEMT) ) {
			xSemaphoreGiveFromISR(uart0.semTx, &xHigherPriorityTaskWoken);
		}
		else {
		}
   }

   portYIELD_FROM_ISR ( xHigherPriorityTaskWoken );
}


/**
 * ----------------------------------------------------------------
 * @fn int32_t UART3_IRQHandler(LPC_USART_T* pUART)
 * @brief handler de la IRQ de la uart 3
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

void UART3_IRQHandler (void) {

   signed portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

   uartQueueTipo_t itemQueue;

   /* levanto el registro de causa de interrupcion */
   uint32_t regIIR = Chip_UART_ReadIntIDReg(uart3.pUART);
   /* levanto RLS */
   uint32_t status = Chip_UART_ReadLineStatus(uart3.pUART);
   uint32_t intId;

   /* pregunto si tengo una IRQ pendiente */
   if(regIIR & UART_IIR_INTSTAT_PEND)
	   return;

   /* copio la causa de irq */
   intId = regIIR & UART_IIR_INTID_MASK;

   switch(intId) {

   case UART_IIR_INTID_RLS:      /*!< Interrupt identification: Receive line interrupt */
	   break;

   case UART_IIR_INTID_RDA:      /*!< Interrupt identification: Receive data available interrupt */

	   if(status & UART_LSR_RDR) { // uartRxReady

		   itemQueue.buf[0] = Chip_UART_ReadByte(uart3.pUART);
		   xQueueSendFromISR(uart3.queRx, &itemQueue, &xHigherPriorityTaskWoken);
	   }
	   else {
		   /* si llego aca es porque hubo algun error en la trama */
	   }
	   break;

	case UART_IIR_INTID_CTI:      /*!< Interrupt identification: Character time-out indicator interrupt */
		/* reinicio fifo de recepcion */
		// Chip_UART_SetupFIFOS(uart3.pUART, UART_FCR_RX_RS);
		break;

	case UART_IIR_INTID_THRE:     /*!< Interrupt identification: THRE interrupt */

		if( (status & UART_LSR_THRE) || (status & UART_LSR_TEMT) ) {
			xSemaphoreGiveFromISR(uart3.semTx, &xHigherPriorityTaskWoken);
		}
		else {
		}
   }

   portYIELD_FROM_ISR ( xHigherPriorityTaskWoken );
}
