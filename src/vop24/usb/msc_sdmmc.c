 /*
 * @brief File contains callback to MSC driver backed by a memory disk.
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include <string.h>
#include "board.h"
#include "app_usbd_cfg.h"

#include "msc_disk.h"
#include "sapi_sdcard.h"

#include "fssdc.h"


#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "uart.h"
#include "apps.h"
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
/*Mass Storage Control Structure for older ROM stack i.e version 0x01111101 */
typedef struct OLD_MSC_CTRL_T
{
  /* If it's a USB HS, the max packet is 512, if it's USB FS,
  the max packet is 64. Use 512 for both HS and FS. */
  /*ALIGNED(4)*/ uint8_t  BulkBuf[USB_HS_MAX_BULK_PACKET]; /* Bulk In/Out Buffer */
  /*ALIGNED(4)*/MSC_CBW CBW;                   /* Command Block Wrapper */
  /*ALIGNED(4)*/MSC_CSW CSW;                   /* Command Status Wrapper */

  USB_CORE_CTRL_T*  pUsbCtrl;

  uint32_t Offset;                  /* R/W Offset */
  uint32_t Length;                  /* R/W Length */
  uint32_t BulkLen;                 /* Bulk In/Out Length */
  uint8_t* rx_buf;

  uint8_t BulkStage;               /* Bulk Stage */
  uint8_t if_num;                  /* interface number */
  uint8_t epin_num;                /* BULK IN endpoint number */
  uint8_t epout_num;               /* BULK OUT endpoint number */
  uint32_t MemOK;                  /* Memory OK */

  uint8_t*  InquiryStr;
  uint32_t  BlockCount;
  uint32_t  BlockSize;
  uint32_t  MemorySize;
  /* user defined functions */
  void (*MSC_Write)( uint32_t offset, uint8_t** src, uint32_t length);
  void (*MSC_Read)( uint32_t offset, uint8_t** dst, uint32_t length);
  ErrorCode_t (*MSC_Verify)( uint32_t offset, uint8_t src[], uint32_t length);
  /* optional call back for MSC_Write optimization */
  void (*MSC_GetWriteBuf)( uint32_t offset, uint8_t** buff_adr, uint32_t length);

}USB_OLD_MSC_CTRL_T;

static volatile int32_t sdio_wait_exit = 0;
/* SDMMC card info structure */
// mci_card_struct sdCardInfo;
volatile uint32_t timerCntms = 0; /* Free running milli second timer */

static USB_EP_HANDLER_T	default_bulk_out_hdlr;
static const uint8_t g_InquiryStr[] = {'N', 'X', 'P', ' ', ' ', ' ', ' ', ' ',	   \
									   'L', 'P', 'C', ' ', 'M', 'e', 'm', ' ',	   \
									   'D', 'i', 's', 'k', ' ', ' ', ' ', ' ',	   \
									   '1', '.', '0', ' ', };
static void	*g_pMscCtrl;
static uint32_t	wrBuffIndex = 0;
static uint32_t	startSector;
static uint32_t	xfer_buff_len;
static uint32_t	total_xfer_len;
static uint8_t	update_xfer_len;

/* In GCC Place it in RamLoc40 */

//#ifdef __GNUC__
//__attribute__((section(".bss.$RamLoc40")))
//#endif
/* Buffer for Mass Storage data */
__attribute__((section(".bss.$RamLoc32")))
ALIGNED(512) uint8_t msc_sd_buf[MSC_BUFF_SIZE];

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

typedef struct {

	bool primerIngreso;
	DWORD ultimoSectorIniCopiado;
	DWORD ultimoSectorFinCopiado;
	DWORD ultimoSectorCantCopiado;
	DWORD sectorUltimo;
} marcasMem_t;

// marcasMem_t marcasBuf_rd;


static bool primerIngreso = false;
static DWORD ultimoSectorIniCopiado;
static DWORD ultimoSectorFinCopiado;
static DWORD ultimoSectorCantCopiado;
static DWORD sectorUltimo;

static void translate_rd(uint32_t offset, uint8_t * *buff_adr, uint32_t length, uint32_t hi_offset)
{
	DWORD sectorIni;
	DWORD sectorFin;
	UINT sectoresCant;
	UINT cantSectoresLeer;

	uint32_t sectorIniOffset;
	uint32_t offsetBuf;


#ifdef DEBUGUSB
	uint8_t msg[64];
//	Board_UARTPutSTR("msc:rd");
//	usbInicializado = true;
//	sprintf(msg, "\toffset:%d\r\n", offset);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\tbuff_adr:%p\r\n", *buff_adr);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\tlength%d\r\n", length);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\thi_offset%d\r\n", hi_offset);
//	Board_UARTPutSTR(msg);
#endif

	if(length == 0) return;

	sectorIni = (((uint64_t) offset) | (((uint64_t) hi_offset) << 32))  >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
	sectorFin = ((((uint64_t) offset) | (((uint64_t) hi_offset) << 32)) + length) >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
	sectoresCant = sectorFin - sectorIni + 1;



#ifdef DEBUGUSB
//	sprintf(msg, "\tultimoSectorIniCopiado%d\r\n", ultimoSectorIniCopiado);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\tultimoSectorFinCopiado%d\r\n", ultimoSectorFinCopiado);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\tultimoSectorCantCopiado%d\r\n", ultimoSectorCantCopiado);
//	Board_UARTPutSTR(msg);
#endif

	if(primerIngreso || (sectorIni > ultimoSectorFinCopiado) || (sectorFin > ultimoSectorFinCopiado) ||	(sectorIni < ultimoSectorIniCopiado)) {
		primerIngreso = false;

#ifdef DEBUGUSB
//		Board_UARTPutSTR("\t\t\t\t>>>leo sd\r\n");
#endif

//		if((sectoresCant << MSC_MEM_DISK_BLOCK_SIZE_EXP) < MSC_BUFF_SIZE) {

		cantSectoresLeer = MSC_BUFF_BLOQUES_N;

		if((sectorIni + cantSectoresLeer) > sectorUltimo)
			cantSectoresLeer = sectorUltimo - sectorIni;

		if(FSSDC_FatFs_DiskRead(msc_sd_buf, sectorIni, cantSectoresLeer) != RES_OK) {
			Board_UARTPutSTR("rd:diskread:error\r\n");
		}
		else {
			ultimoSectorCantCopiado = cantSectoresLeer;
			ultimoSectorIniCopiado = sectorIni;
			ultimoSectorFinCopiado = sectorIni + cantSectoresLeer;
		}
	}
	sectorIniOffset = ultimoSectorIniCopiado << MSC_MEM_DISK_BLOCK_SIZE_EXP;
	offsetBuf = offset - sectorIniOffset;
	*buff_adr = msc_sd_buf + offsetBuf;

#ifdef DEBUGUSB
//	sprintf(msg, "\tsectorIni:%d\r\n", sectorIni);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\tsectorFin:%d\r\n", sectorFin);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\tsectoresCant%d\r\n", sectoresCant);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\tsectorIniOffset%d\r\n", sectorIniOffset);
//	Board_UARTPutSTR(msg);
//	sprintf(msg, "\toffsetBuf%d\r\n", offsetBuf);
//	Board_UARTPutSTR(msg);
#endif

	return 0;


}


static void translate_wr(uint32_t offset, uint8_t * *buff_adr, uint32_t length, uint32_t hi_offset)
{

	DWORD sectorIni;
	DWORD sectorFin;
	UINT sectoresCant;
	UINT cantSectoresLeer;

	uint32_t sectorIniOffset;
	uint32_t offsetBuf;


	if(length == 0) return;

	sectorIni = (((uint64_t) offset) | (((uint64_t) hi_offset) << 32))  >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
	sectorFin = ((((uint64_t) offset) | (((uint64_t) hi_offset) << 32)) + length) >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
	sectoresCant = sectorFin - sectorIni + 1;


	if(primerIngreso || (sectorIni > ultimoSectorFinCopiado) || (sectorFin > ultimoSectorFinCopiado) ||	(sectorIni < ultimoSectorIniCopiado)) {
		primerIngreso = false;
		cantSectoresLeer = MSC_BUFF_BLOQUES_N;

		if((sectorIni + cantSectoresLeer) > sectorUltimo)
			cantSectoresLeer = sectorUltimo - sectorIni;

		if(FSSDC_FatFs_DiskRead(msc_sd_buf, sectorIni, cantSectoresLeer) != RES_OK) {
			Board_UARTPutSTR("rd:diskread:error\r\n");
		}
		else {
			ultimoSectorCantCopiado = cantSectoresLeer;
			ultimoSectorIniCopiado = sectorIni;
			ultimoSectorFinCopiado = sectorIni + cantSectoresLeer;
		}
	}
	sectorIniOffset = ultimoSectorIniCopiado << MSC_MEM_DISK_BLOCK_SIZE_EXP;
	offsetBuf = offset - sectorIniOffset;
	*buff_adr = msc_sd_buf + offsetBuf + length;


}


/* USB device mass storage class write callback routine */
//static void translate_wr(uint32_t offset, uint8_t * *buff_adr, uint32_t length, uint32_t hi_offset)
//{
//#ifdef DEBUGUSB
//	Board_UARTPutSTR("msc:wr");
//#endif
//
//	if(USBD_API->version == 0x01111101) {
//		/* No high offset for older stack */
//		hi_offset = 0;
//	}
//	/* If a new write request has started then copy total transfer length from control structure
//		 and calculate the data block size for SD write */
//	if(update_xfer_len) {
//		/* Start sector for the write request is updated */
//		startSector = (((uint64_t) offset) | (((uint64_t) hi_offset) << 32))>>  MSC_MEM_DISK_BLOCK_SIZE_EXP;
//		if(USBD_API->version > 0x01111101) {
//			/* New ROM stack version, use new control structure */
//			total_xfer_len = ((USB_MSC_CTRL_T *)g_pMscCtrl)->Length;
//		}
//		else {
//			/* Old ROM stack version, use old control structure */
//			total_xfer_len = ((USB_OLD_MSC_CTRL_T *)g_pMscCtrl)->Length;
//		}
//		/* Transfer size from SD card is the minimum of buffer size and total transfer length */
//		if(total_xfer_len > MSC_BUFF_SIZE) {
//			xfer_buff_len = MSC_BUFF_SIZE;
//		}
//		else {
//			xfer_buff_len = total_xfer_len;
//		}
//		wrBuffIndex = 0;
//		update_xfer_len = 0;
//	}
//	/* Increment the index for the buffer */
//	wrBuffIndex += length;
//	/* When entire buffer is written, write the data block to the SD card */
//	if(wrBuffIndex == xfer_buff_len) {
//		FSSDC_FatFs_DiskWrite (&msc_sd_buf[0], startSector, (xfer_buff_len + MSC_MEM_DISK_BLOCK_SIZE - 1) >> MSC_MEM_DISK_BLOCK_SIZE_EXP);
//		// Chip_SDMMC_WriteBlocks(LPC_SDMMC, &msc_sd_buf[0], startSector, (xfer_buff_len + MMC_SECTOR_SIZE - 1)/MMC_SECTOR_SIZE);
//		/* Reset index*/
//		wrBuffIndex = 0;
//		/* Update the start sector, total transfer length and data block size for SD write */
//		startSector += (xfer_buff_len + MSC_MEM_DISK_BLOCK_SIZE - 1) >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
//		total_xfer_len -= xfer_buff_len;
//		if(total_xfer_len > MSC_BUFF_SIZE) {
//			xfer_buff_len = MSC_BUFF_SIZE;
//		}
//		else {
//			xfer_buff_len = total_xfer_len;
//		}
//	}
//	/* Data pointer to the next USB bulk OUT packet */
//	*buff_adr =  &msc_sd_buf[wrBuffIndex];
//}

/* USB device mass storage class get write buffer callback routine */
static void translate_GetWrBuf(uint32_t offset, uint8_t * *buff_adr, uint32_t length, uint32_t hi_offset)
{
	DWORD sectorIni;
	DWORD sectorFin;
	UINT sectoresCant;
	UINT cantSectoresLeer;

	uint32_t sectorIniOffset;
	uint32_t offsetBuf;

	Board_UARTPutSTR("msc:GetWrBuf");
#ifdef DEBUGUSB
	uint8_t msg[64];
	usbInicializado = true;
	sprintf(msg, "\toffset:%d\r\n", offset);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\tbuff_adr:%p\r\n", *buff_adr);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\tlength%d\r\n", length);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\thi_offset%d\r\n", hi_offset);
	Board_UARTPutSTR(msg);
#endif

	if(length == 0) return;

	sectorIni = (((uint64_t) offset) | (((uint64_t) hi_offset) << 32))  >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
	sectorFin = ((((uint64_t) offset) | (((uint64_t) hi_offset) << 32)) + length) >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
	sectoresCant = sectorFin - sectorIni + 1;

#ifdef DEBUGUSB
	sprintf(msg, "\tultimoSectorIniCopiado%d\r\n", ultimoSectorIniCopiado);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\tultimoSectorFinCopiado%d\r\n", ultimoSectorFinCopiado);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\tultimoSectorCantCopiado%d\r\n", ultimoSectorCantCopiado);
	Board_UARTPutSTR(msg);
#endif

	if(primerIngreso || (sectorIni > ultimoSectorFinCopiado) || (sectorFin > ultimoSectorFinCopiado) ||	(sectorIni < ultimoSectorIniCopiado)) {
		primerIngreso = false;

#ifdef DEBUGUSB
		Board_UARTPutSTR("\t\t\t\t>>>leo sd\r\n");
#endif

		cantSectoresLeer = MSC_BUFF_BLOQUES_N;

		if((sectorIni + cantSectoresLeer) > sectorUltimo)
			cantSectoresLeer = sectorUltimo - sectorIni;

		if(FSSDC_FatFs_DiskRead(msc_sd_buf, sectorIni, cantSectoresLeer) != RES_OK) {
			Board_UARTPutSTR("rd:diskread:error\r\n");
		}
		else {
			ultimoSectorCantCopiado = cantSectoresLeer;
			ultimoSectorIniCopiado = sectorIni;
			ultimoSectorFinCopiado = sectorIni + cantSectoresLeer;
		}
	}
	sectorIniOffset = ultimoSectorIniCopiado << MSC_MEM_DISK_BLOCK_SIZE_EXP;
	offsetBuf = offset - sectorIniOffset;
	*buff_adr = msc_sd_buf + offsetBuf;

#ifdef DEBUGUSB
	sprintf(msg, "\tsectorIni:%d\r\n", sectorIni);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\tsectorFin:%d\r\n", sectorFin);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\tsectoresCant%d\r\n", sectoresCant);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\tsectorIniOffset%d\r\n", sectorIniOffset);
	Board_UARTPutSTR(msg);
	sprintf(msg, "\toffsetBuf%d\r\n", offsetBuf);
	Board_UARTPutSTR(msg);
#endif

	return 0;
}

/* USB device mass storage class verify callback routine */
static ErrorCode_t translate_verify(uint32_t offset, uint8_t *src, uint32_t length, uint32_t hi_offset)
{
#ifdef DEBUGUSB
#endif
	Board_UARTPutSTR("msc:verify");
	if(USBD_API->version == 0x01111101) {
		/* No high offset for older stack */
		hi_offset = 0;
	}
	/* If a new RW request has started then copy total transfer length from control structure
		 and read a new block of data from SD card */
	if(update_xfer_len) {
		/* Start sector for the read request is updated */
		startSector = (((uint64_t) offset) | (((uint64_t) hi_offset) << 32))>> MSC_MEM_DISK_BLOCK_SIZE_EXP;
		if(USBD_API->version > 0x01111101) {
			/* New ROM stack version, use new control structure */
			total_xfer_len = ((USB_MSC_CTRL_T *)g_pMscCtrl)->Length;
		}
		else {
			/* Old ROM stack version, use old control structure */
			total_xfer_len = ((USB_OLD_MSC_CTRL_T *)g_pMscCtrl)->Length;
		}
		/* Transfer size from SD card is the minimum of buffer size and total transfer length */
		if(total_xfer_len > MSC_BUFF_SIZE) {
			xfer_buff_len = MSC_BUFF_SIZE;
		}
		else {
			xfer_buff_len = total_xfer_len;
		}
		update_xfer_len = 0;
		/* Read from SD card */
		FSSDC_FatFs_DiskRead(&msc_sd_buf[0], startSector, (xfer_buff_len + MSC_MEM_DISK_BLOCK_SIZE - 1)>> MSC_MEM_DISK_BLOCK_SIZE_EXP);
		// Chip_SDMMC_ReadBlocks(LPC_SDMMC, &msc_sd_buf[0], startSector, (xfer_buff_len + MMC_SECTOR_SIZE - 1)/MMC_SECTOR_SIZE);
	}
	/* For a previous request when the buffered data is read out completely then refill the buffer with next set of data */
	else if(((((uint64_t) offset) | (((uint64_t) hi_offset) << 32)) - (startSector << MSC_MEM_DISK_BLOCK_SIZE_EXP)) == xfer_buff_len) {
		/* Update the start sector, total transfer length and data length to be read from SD card */
		startSector += (xfer_buff_len + MSC_MEM_DISK_BLOCK_SIZE - 1)>> MSC_MEM_DISK_BLOCK_SIZE_EXP;
		total_xfer_len -= xfer_buff_len;
		if(total_xfer_len > MSC_BUFF_SIZE) {
			xfer_buff_len = MSC_BUFF_SIZE;
		}
		else {
			xfer_buff_len = total_xfer_len;
		}
		/* Read next set of data from SD card */
		FSSDC_FatFs_DiskRead(&msc_sd_buf[0], startSector, (xfer_buff_len + MSC_MEM_DISK_BLOCK_SIZE - 1)>> MSC_MEM_DISK_BLOCK_SIZE_EXP);
		// Chip_SDMMC_ReadBlocks(LPC_SDMMC, &msc_sd_buf[0], startSector, (xfer_buff_len + MMC_SECTOR_SIZE - 1)/MMC_SECTOR_SIZE);
	}
	/* Compare data return accordingly*/
	if (memcmp((void *) &msc_sd_buf[(((uint64_t) offset) | (((uint64_t) hi_offset) << 32)) - (startSector * MSC_MEM_DISK_BLOCK_SIZE)], src, length)) {
		return ERR_FAILED;
	}

	return LPC_OK;
}

/* Override for Bulk out handler */
static ErrorCode_t app_bulk_out_hdlr(USBD_HANDLE_T hUsb, void* data, uint32_t event)
{

	uint8_t msg[64];

	Board_UARTPutSTR("bulkout\r\n");
#ifdef DEBUGUSB
#endif
	void *pMscCtrl = data;
	if(USBD_API->version > 0x01111101) {
		/* If a new RW request is received then set flag for updating total transfer length */
		if( (event == USB_EVT_OUT) && (((USB_MSC_CTRL_T*)pMscCtrl)->BulkStage == MSC_BS_CBW) ) {
			update_xfer_len = 1;
			sprintf("BulkLen:%d\r\n", ((USB_MSC_CTRL_T*)pMscCtrl)->BulkLen);
			Board_UARTPutSTR(msg);
#ifdef DEBUGUSB
			Board_UARTPutSTR("bulkout:update=1\r\n");
#endif
		}
	}
	else {
		/* If a new RW request is received then set flag for updating total transfer length */
		if( (event == USB_EVT_OUT) && (((USB_OLD_MSC_CTRL_T*)pMscCtrl)->BulkStage == MSC_BS_CBW) ) {
			update_xfer_len = 1;
			sprintf("BulkLen2:%d\r\n", ((USB_MSC_CTRL_T*)pMscCtrl)->BulkLen);
			Board_UARTPutSTR(msg);
		}
	}
	/* Call the default handler */
  return default_bulk_out_hdlr(hUsb, data, event);
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Memory storage based MSC_Disk init routine */
ErrorCode_t mscDisk_init(USBD_HANDLE_T hUsb, USB_CORE_DESCS_T *pDesc, USBD_API_INIT_PARAM_T *pUsbParam)
{
	char msg[100];

	USBD_MSC_INIT_PARAM_T msc_param;
	ErrorCode_t ret = LPC_OK;
	USB_CORE_CTRL_T* pCtrl = (USB_CORE_CTRL_T*)hUsb;

	memset((void *) &msc_param, 0, sizeof(USBD_MSC_INIT_PARAM_T));

	msc_param.mem_base = pUsbParam->mem_base;
	msc_param.mem_size = pUsbParam->mem_size;
	g_pMscCtrl = (void *)msc_param.mem_base;

	/* mass storage paramas */
	msc_param.InquiryStr = (uint8_t *) g_InquiryStr;
	msc_param.BlockCount = (sdcard.fatFs.n_fatent - 2) * sdcard.fatFs.csize + sdcard.fatFs.database;
	msc_param.BlockSize = MSC_MEM_DISK_BLOCK_SIZE;
	// msc_param.Length = MSC_BUFF_SIZE;

	/*Update memory size based on the stack version */
	if(USBD_API->version > 0x01111101) {
		/* New ROM stack version */
		msc_param.MemorySize64 = msc_param.BlockCount*MSC_MEM_DISK_BLOCK_SIZE;
		msc_param.MemorySize = 0;
	}
	else {
		/* Old ROM stack version - cannot support more than 4GB of memory card */
		msc_param.MemorySize = msc_param.BlockCount*MSC_MEM_DISK_BLOCK_SIZE;
	}

	/* Install memory storage callback routines */
	msc_param.MSC_Write = translate_wr;
	msc_param.MSC_Read = translate_rd;
	msc_param.MSC_Verify = translate_verify;
	msc_param.MSC_GetWriteBuf = translate_GetWrBuf;

	msc_param.intf_desc = (uint8_t *) find_IntfDesc(pDesc->high_speed_desc, USB_DEVICE_CLASS_STORAGE);

	primerIngreso = false;
	sectorUltimo = (sdcard.fatFs.n_fatent - 2) * sdcard.fatFs.csize + sdcard.fatFs.database;


	if(FSSDC_FatFs_DiskRead(msc_sd_buf, 0, MSC_BUFF_SIZE >> MSC_MEM_DISK_BLOCK_SIZE_EXP) != RES_OK)
		Board_UARTPutSTR("init:diskread:error\r\n");

	ultimoSectorCantCopiado = MSC_BUFF_SIZE >> MSC_MEM_DISK_BLOCK_SIZE_EXP;
	ultimoSectorIniCopiado = 0;
	ultimoSectorFinCopiado = 0 + ultimoSectorCantCopiado;


//#define DEBUGSDINFO
#ifdef DEBUGSDINFO
	sprintf(msg, "Filesystem type (0:N/A) - fs_type:%d\r\n", sdcard.fatFs.fs_type);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Physical drive number - pdrv :%d\r\n", sdcard.fatFs.pdrv);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Number of FATs (1 or 2) n_fats:%d\r\n", sdcard.fatFs.n_fats);
	Board_UARTPutSTR(msg);
	sprintf(msg, "win[] flag (b0:dirty) :%d\r\n", sdcard.fatFs.wflag);
	Board_UARTPutSTR(msg);
	sprintf(msg, "FSINFO flags (b7:disabled, b0:dirty) fsi_flag:%d\r\n", sdcard.fatFs.fsi_flag);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Volume mount ID id:%d\r\n", sdcard.fatFs.id);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Number of root directory entries (FAT12/16) n_rootdir:%d\r\n", sdcard.fatFs.n_rootdir);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Cluster size [sectors] - csize:%d\r\n", sdcard.fatFs.csize);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Last allocated cluster - last_clst:%d\r\n", sdcard.fatFs.last_clst);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Number of free clusters - :%d\r\n", sdcard.fatFs.free_clst);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Number of FAT entries (number of clusters + 2) - :%d\r\n", sdcard.fatFs.n_fatent);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Size of an FAT [sectors] - :%d\r\n", sdcard.fatFs.fsize);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Volume base sector - volbase:%d\r\n", sdcard.fatFs.volbase);
	Board_UARTPutSTR(msg);
	sprintf(msg, "FAT base sector - :%d\r\n", sdcard.fatFs.fatbase);
	Board_UARTPutSTR(msg);

	sprintf(msg, "Root directory base sector/cluster - dirbase:%d\r\n", sdcard.fatFs.dirbase);
	Board_UARTPutSTR(msg);
	sprintf(msg, "Data base sector - database:%d\r\n", sdcard.fatFs.database);
	Board_UARTPutSTR(msg);
#endif

	if(USBD_API->msc->init(hUsb, &msc_param) != LPC_OK)
		Board_UARTPutSTR("init:error msc init\r\n");


	update_xfer_len = 0;

	if(USBD_API->version > 0x01111101) {
		/* New ROM stack version */
		default_bulk_out_hdlr = pCtrl->ep_event_hdlr[(((USB_MSC_CTRL_T *)g_pMscCtrl)->epout_num & 0x0F) << 1];
		USBD_API->core->RegisterEpHandler(hUsb, (((USB_MSC_CTRL_T *)g_pMscCtrl)->epout_num & 0x0F) << 1, app_bulk_out_hdlr, g_pMscCtrl);
	}
	else {
		/* Old ROM stack version */
		default_bulk_out_hdlr = pCtrl->ep_event_hdlr[(((USB_OLD_MSC_CTRL_T *)g_pMscCtrl)->epout_num & 0x0F) << 1];
		USBD_API->core->RegisterEpHandler(hUsb, (((USB_OLD_MSC_CTRL_T *)g_pMscCtrl)->epout_num & 0x0F) << 1, app_bulk_out_hdlr, g_pMscCtrl);
	}
	/* update memory variables */
	pUsbParam->mem_base = msc_param.mem_base;
	pUsbParam->mem_size = msc_param.mem_size;

	return ret;
}
