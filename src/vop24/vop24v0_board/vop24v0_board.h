/**
 * ============================================================
 * @file vop24v0_board.h
 * @brief header del archivo vop24v0_board.c
 * @author fede (rouxfederico@gmail.com)
 * @date 09/08/2019
 * ============================================================
 */

#ifndef SRC_VOP24V0_BOARD_VOP24V0_BOARD_H_
#define SRC_VOP24V0_BOARD_VOP24V0_BOARD_H_

/* -------------------------- pines generales ------------------------------------*/

/* redefinicion de entrada y salida */
#define GPIO_DIR_ENTRADA 					false
#define GPIO_DIR_SALIDA 					true

/* redefinicion de estado de pines */
#define GPIO_EST_BAJO						false
#define GPIO_EST_ALTO						true

/* define de pines de la placa */

/** @brief TPD EN habilitacion activo bajo del TPD4S014 - proteccion de USB */
#define TPD_EN_PORT							0
#define TPD_EN_PIN							28
#define TPD_EN_DIR							GPIO_DIR_SALIDA
#define TPD_EN_EST_INI						GPIO_EST_BAJO

/** @brief TPD ACK entrada activo bajo del TPD4S014 que indica cuando se conecto el VUSB */
#define TPD_ACK_PORT						0
#define TPD_ACK_PIN							27
#define TPD_ACK_DIR							GPIO_DIR_ENTRADA


/** @brief LT1512 S-S habilitacion activo alto del LT1512 - carga batería por USB */
#define LT1512_SS_PORT						2
#define LT1512_SS_PIN						6
#define LT1512_SS_DIR						GPIO_DIR_SALIDA
#define LT1512_SS_EST_INI					GPIO_EST_ALTO

/** @brief LTC4411 STAT entrada que indica si esta por batería o por USB */
#define LTC4411_STAT_PORT					2
#define LTC4411_STAT_PIN					27
#define LTC4411_STAT_DIR					GPIO_DIR_ENTRADA


/* -------------------------- pines usb ------------------------------------*/

/** @brief USB ID no se usa, esta conectada al TPD4S014 pero queda flotando */
#define USB_ID_PORT							3
#define USB_ID_PIN							25
#define USB_ID_DIR							GPIO_DIR_ENTRADA

/** @brief USB VBUS entrada que recibe */
#define USB_VBUS_PORT						1
#define USB_VBUS_PIN						30
#define USB_VBUS_IOCON_MODE					IOCON_MODE_INACT		/* <<<< este es muy importante que quede asi */
#define USB_VBUS_IOCON_FUNC					IOCON_FUNC0

/** @brief USB UP LED salida por hardware que indica cuando activó por VBUS*/
#define USB_UP_LED_PORT						1
#define USB_UP_LED_PIN						18
#define USB_UP_LED_IOCON_MODE				IOCON_MODE_INACT		/* <<<< este es muy importante que quede asi */
#define USB_UP_LED_IOCON_FUNC				IOCON_FUNC1

/** @brief USB CONNECT salida que activa pull up de USB-D+ */
#define USB_CONNECT_PORT					2
#define USB_CONNECT_PIN						9
#define USB_CONNECT_IOCON_MODE				IOCON_MODE_INACT		/* <<<< este es muy importante que quede asi */
#define USB_CONNECT_IOCON_FUNC				IOCON_FUNC1

/** @brief USB D+ pin del USB */
#define USB_DP_PORT							0
#define USB_DP_PIN							29
#define USB_DP_IOCON_MODE					IOCON_MODE_INACT		/* <<<< este es muy importante que quede asi */
#define USB_DP_IOCON_FUNC					IOCON_FUNC1

/** @brief USB D- pin del USB */
#define USB_DN_PORT							0
#define USB_DN_PIN							30
#define USB_DN_IOCON_MODE					IOCON_MODE_INACT		/* <<<< este es muy importante que quede asi */
#define USB_DN_IOCON_FUNC					IOCON_FUNC1


/* -------------------------- funciones externas ------------------------------------*/

extern int32_t InicializarPinesVOP24v0_Board (void);
extern int32_t InicializarPinesUSB_VOP24v0_Board(void);


#endif /* SRC_VOP24V0_BOARD_VOP24V0_BOARD_H_ */

