/**
 * ============================================================
 * @file   vop24v0_doard.c
 * @brief  funciones de habilitación de pines de la placa
 * @author fede (rouxfederico@gmail.com)
 * @date 09/08/2019
 * ============================================================
 */

/* ----------------------------------- includes ------------------------------------------ */


#include "stdint.h"
#include "board.h"

#include "vop24v0_board.h"

/* ----------------------------------- prototipos ------------------------------------------ */

int32_t InicializarPinesVOP24v0_Board (void);
int32_t InicializarPinesUSB_VOP24v0_Board(void);

/* ----------------------------------- funciones ------------------------------------------ */

/**
 * ----------------------------------------------------------------
 * @fn int32_t InicializarPinesVOP24v0_Board(void)
 * @brief Inicializo pines de la placa VOP24 v0
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t InicializarPinesVOP24v0_Board (void) {

	/* TPD EN */
	Chip_IOCON_PinMux(LPC_IOCON, TPD_EN_PORT, TPD_EN_PIN, IOCON_MODE_INACT, IOCON_FUNC0);
	Chip_GPIO_SetPinDIR(LPC_GPIO, TPD_EN_PORT, TPD_EN_PIN, TPD_EN_DIR);
	Chip_GPIO_SetPinState(LPC_GPIO, TPD_EN_PORT, TPD_EN_PIN, TPD_EN_EST_INI);

	/* TPD ACK */
	Chip_IOCON_PinMux(LPC_IOCON, TPD_ACK_PORT, TPD_ACK_PIN, IOCON_MODE_INACT, IOCON_FUNC0);
	Chip_GPIO_SetPinDIR(LPC_GPIO, TPD_ACK_PORT, TPD_ACK_PIN, TPD_ACK_DIR);

	/* LT1512 s-s */
	Chip_IOCON_PinMux(LPC_IOCON, LT1512_SS_PORT, LT1512_SS_PIN, IOCON_MODE_INACT, IOCON_FUNC0);
	Chip_GPIO_SetPinDIR(LPC_GPIO, LT1512_SS_PORT, LT1512_SS_PIN, LT1512_SS_DIR);
	Chip_GPIO_SetPinState(LPC_GPIO, LT1512_SS_PORT, LT1512_SS_PIN, LT1512_SS_EST_INI);

	/* LTC4411 - STAT */
	Chip_IOCON_PinMux(LPC_IOCON, LTC4411_STAT_PORT, LTC4411_STAT_PIN, IOCON_MODE_INACT, IOCON_FUNC0);
	Chip_GPIO_SetPinDIR(LPC_GPIO, LTC4411_STAT_PORT, LTC4411_STAT_PIN, LTC4411_STAT_DIR);

	return 0;

}


/**
 * ----------------------------------------------------------------
 * @fn int32_t InicializarPinesUSB_VOP24v0_Board(void)
 * @brief Inicializo pines del USB de la placa VOP24 v0
 * @author fede (rouxfederico@gmail.com)
 * ----------------------------------------------------------------
 */

int32_t InicializarPinesUSB_VOP24v0_Board(void) {

	/* entada USB ID */
	Chip_IOCON_PinMux(LPC_IOCON, USB_ID_PORT, USB_ID_PIN, IOCON_MODE_INACT, IOCON_FUNC0);
	Chip_GPIO_SetPinDIR(LPC_GPIO, USB_ID_PORT, USB_ID_PIN, USB_ID_DIR);

	/* USB VBUS */
	Chip_IOCON_PinMux(LPC_IOCON, USB_VBUS_PORT, USB_VBUS_PIN, USB_VBUS_IOCON_MODE, USB_VBUS_IOCON_FUNC);

	/* P1.18 - USB_UP_LED    	  <<	01   PAD1	*/
	Chip_IOCON_PinMux(LPC_IOCON, USB_UP_LED_PORT, USB_UP_LED_PIN, USB_UP_LED_IOCON_MODE, USB_UP_LED_IOCON_FUNC);

	Chip_IOCON_PinMux(LPC_IOCON, USB_CONNECT_PORT, USB_CONNECT_PIN, USB_CONNECT_IOCON_MODE, USB_CONNECT_IOCON_FUNC); 	/* 2.9	USB_CONNECT 	  <<	PAD19   01	pull up externo con transistor en placa lpcxpresso */

	/* P0.29 D1+, P0.30 D1- */
	Chip_IOCON_PinMux(LPC_IOCON, USB_DP_PORT, USB_DP_PIN, USB_DP_IOCON_MODE, USB_DP_IOCON_FUNC);
	Chip_IOCON_PinMux(LPC_IOCON, USB_DN_PORT, USB_DN_PIN, USB_DN_IOCON_MODE, USB_DN_IOCON_FUNC);

	LPC_USB->USBClkCtrl = 0x12;                /* Dev, AHB clock enable */
	while ((LPC_USB->USBClkSt & 0x12) != 0x12);


	return 0;
}
