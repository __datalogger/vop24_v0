/*
 * timer.c
 *
 *  Created on: 21 sep. 2019
 *      Author: froux
 */

/*========================== inclusion de archivos ==========================*/
#include "board.h"
#include <cr_section_macros.h>

#include "FreeRTOS.h"
#include "semphr.h"


/*========================== defines  ==========================*/

#define TIMER0_MATCH_REG	1

/*========================== prototipos de fcs ==========================*/

int32_t InicializarTimer (int32_t frecuenciaHz);
int32_t TimerDelayTicks (int32_t ticks);


/*========================== variables generales ==========================*/

volatile bool finCuenta = false;
volatile int32_t cuenta;
volatile int32_t cuentaMax;

/*========================== implementacion de fcs ==========================*/
/**
==============================================================
@fn 		int InicializarTimer (void)
@brief		inicializacion del timer para disparar el adc
@paramin 	void
@paramout 	0 success
@author 	fede(rouxfederico@gmail.com)
==============================================================
*/

int32_t InicializarTimer (int32_t frecuenciaHz) {

	uint32_t timerFreq;

	/* selecciono el divisor PCLK en 1 */
	Chip_Clock_SetPCLKDiv(SYSCTL_PCLK_TIMER0, 01);

	/* Enable timer 1 clock */
	Chip_TIMER_Init(LPC_TIMER0);

	/* Timer rate is system clock rate */
	timerFreq = Chip_Clock_GetSystemClockRate();

	/* Timer setup for match and interrupt at TICKRATE_HZ */
	Chip_TIMER_Reset(LPC_TIMER0);
	Chip_TIMER_SetMatch(LPC_TIMER0, TIMER0_MATCH_REG, (timerFreq / frecuenciaHz));
//	Chip_TIMER_MatchEnableInt(LPC_TIMER0, TIMER0_MATCH_REG);
	// Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, TIMER0_MATCH_REG);
	/* esto es muy importante si quiero disparar el adc desde el timer */
	// Chip_TIMER_ExtMatchControlSet(LPC_TIMER0, 0, TIMER_EXTMATCH_TOGGLE, TIMER0_MATCH_REG);

	finCuenta = false;
//	/* Enable timer interrupt */
//	NVIC_ClearPendingIRQ(TIMER0_IRQn);
//	NVIC_EnableIRQ(TIMER0_IRQn);

	return 0;
}

/**
==============================================================
@fn 		int32_t TimerDelayTicks (int32_t ticks)
@brief		delay bloqueante en cantidad de ticks
@paramin 	void
@paramout 	0 success
@author 	fede(rouxfederico@gmail.com)
==============================================================
*/

int32_t TimerDelayTicks (int32_t ticks) {


	if(ticks <= 0) return -1;

	cuentaMax = ticks;
	cuenta = 0;
	finCuenta = false;


	Chip_TIMER_MatchEnableInt(LPC_TIMER0, TIMER0_MATCH_REG);
	Chip_TIMER_Enable(LPC_TIMER0);
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, TIMER0_MATCH_REG);

	/* Enable timer interrupt */
	NVIC_ClearPendingIRQ(TIMER0_IRQn);
	NVIC_EnableIRQ(TIMER0_IRQn);


	while(finCuenta == false);

	Chip_TIMER_MatchDisableInt(LPC_TIMER0, TIMER0_MATCH_REG);
	Chip_TIMER_Disable(LPC_TIMER0);
	/* Enable timer interrupt */
	NVIC_ClearPendingIRQ(TIMER0_IRQn);
	NVIC_DisableIRQ(TIMER0_IRQn);

	return 0;
}





/* ============= handlers de irq ============= */

/**
===================================================
 @fn 		void TIMER0_IRQHandler(void)
 @brief		Handle interrupt from 32-bit timer
 @return	Nothing
===================================================
*/
void TIMER0_IRQHandler(void)
{
	if (Chip_TIMER_MatchPending(LPC_TIMER0, TIMER0_MATCH_REG)) {
		Chip_TIMER_ClearMatch(LPC_TIMER0, TIMER0_MATCH_REG);

		cuenta++;

		if(cuenta >= cuentaMax)
			finCuenta = true;

	}
}
