/*
 * timer.h
 *
 *  Created on: 21 sep. 2019
 *      Author: froux
 */

#ifndef TIMERS_H_
#define TIMERS_H_

/*========================== funciones globales ==========================*/

extern int32_t InicializarTimer (int32_t frecuenciaHz);
extern int32_t TimerDelayTicks (int32_t ticks);


#endif /* TIMERS_H_ */
