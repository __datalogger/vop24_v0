﻿/**
 @file ads1292.c
 @brief Driver y Handlers del integrado de adquisición de señales ADS1292
 @author Federico Roux
 @date 2013.11.15
**/


/* ==================================================================================
 * 		INCLUSIÓN DE ARCHIVOS :
 * ================================================================================== */


#include "stdint.h"

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "ads1292.h"

#include "tipos.h"

#include "ff.h"
#include "timers.h"

#define DEBUGUART

#ifdef DEBUGUART
#include "uart.h"
#endif

#include "apps.h"

#define DATOS_LISTOS_PRIORIDAD						(tskIDLE_PRIORITY + 1UL)
#define DATOS_LISTOS_STACK							(configMINIMAL_STACK_SIZE * (1UL))


uint8_t empezo_read_ssp_ADS = 1;
uint8_t datosPerdidos = 0;
uint32_t datosOk = 0;
/* ==================================================================================
 *		PROTOTIPOS DE FUNCIONES :
 * ================================================================================== */

void ADS1292_CanalInicializarEstructura (t_canal* canal);
__inline void ADS1292_CanalIntercambiarPunteros (t_canal* canal) __attribute__((always_inline));


int32_t ADS1292_Inicializar (void);

static int32_t ADS1292_ConfigurarCanalSSP( LPC_SSP_T *pSSP );
static uint32_t ADS1292_EnviarComandoCorto(cmdId_t cmdId);
static uint8_t ADS1292_EscribirUnRegistro( uint8_t registro, uint8_t datos);
static uint8_t ADS1292_LeerUnRegistro( uint8_t registro, uint8_t* byteLeido);

void ADS1292_Iniciar_Adquisicion ( void );


/* ==================================================================================
 *		LISTA DE TAREAS Y SU INSTALADOR :
 * ==================================================================================*/

int32_t ADS1292_InicializarPines (void);

/* Tarea de handler del pin DRDY del ADS1292 */
int32_t ADS1292_InstalarTareaDatosListos (void);

void tADS1292_DatosListos ( void *pvParameters );

/* Tarea de ejemplo para disparar la adquisición : */
void InstalarEjemploIniciaAdquisicion (void);
void tEjemploIniciaAdquisicion ( void *pvParameters );

/* ==================================================================================
 *		DEFINICION DE VARIABLES GLOBALES :
 * ==================================================================================*/

/* Semáforo binario para manejar la tarea que atiende el pin DR del ADS1292 */
xSemaphoreHandle vSemaforoBin_ADS1292_DRDY = NULL;
/* Semáforo binario para manejar la taera que atiende el almacenamiento de los buffers de cada canal */
xSemaphoreHandle vSemaforoBinBuffersCompletos = NULL;

/* Buffer de entrada de lectura del ADS1292 */
uint8_t ADS1292_Datos_Leidos [ADS1292_RDATAC_CANTBYTESLEIDOS];

/* Estructuras de canales de adquisición : */
t_canal canal0;
t_canal canal1;

/* Flags del ADS1292 */
volatile uint8_t ADS1292_inicializado = 0;
volatile uint8_t ADS1292_adquiriendo = 0;

uint8_t ads1292_cmdOp[] = {ADS_WAKEUP, ADS_STANDBY, ADS_RESET, ADS_START, ADS_STOP,
							ADS_OFFSETCAL, ADS_RDATAC, ADS_SDATAC, ADS_RDATA,
							ADS_RREG, ADS_WREG};

uint8_t ads1292_cmdDelay[] = {ADS_WAKEUP_DELAY, ADS_STANDBY_DELAY, ADS_RESET_DELAY, ADS_START_DELAY, ADS_STOP_DELAY,
								ADS_OFFSETCAL_DELAY, ADS_RDATAC_DELAY, ADS_SDATAC_DELAY, ADS_RDATA_DELAY,
								ADS_RREG_DELAY, ADS_WREG_DELAY};

/*======================================================================================================
 * 									FUNCIONES COMUNES :
 *======================================================================================================*/

/**
===============================================================================
@fn 		static int32_t ADS1292_SSP_Enviar(uint8_t *buf, uint32_t Length)
@brief		envio bytes por el puerto spi
@paramin 	uint8_t * buf 		buffer con los bytes a enviar
@paramin 	uint32_t Length 	cantidad de bytes a enviar
@paramout 	0 success
@author 	fede(rouxfederico@gmail.com)
===============================================================================
*/

static int32_t ADS1292_SSP_Enviar(uint8_t *buf, uint32_t Length)
{
    Chip_SSP_DATA_SETUP_T xferConfig;

	xferConfig.tx_data = buf;
	xferConfig.tx_cnt  = 0;
	xferConfig.rx_data = NULL;
	xferConfig.rx_cnt  = 0;
	xferConfig.length  = Length;

	if(Chip_SSP_RWFrames_Blocking(ADS1292_SSP_PORT, &xferConfig) == Length)
		return 0;

	return 1;
}

/**
===============================================================================
@fn 		static int32_t ADS1292_SSP_Recibir(uint8_t *buf, uint32_t Length)
@brief		recibo bytes por el puerto spi
@paramin 	uint8_t * buf 		buffer para recibir bytes
@paramin 	uint32_t Length 	cantidad de bytes a enviar
@paramout 	0 success
@author 	fede(rouxfederico@gmail.com)
===============================================================================
*/

static int32_t ADS1292_SSP_Recibir(uint8_t *buf, uint32_t Length )
{
    Chip_SSP_DATA_SETUP_T xferConfig;

	xferConfig.tx_data = NULL;
	xferConfig.tx_cnt  = 0;
	xferConfig.rx_data = buf;
	xferConfig.rx_cnt  = 0;
	xferConfig.length  = Length;

	if(Chip_SSP_RWFrames_Blocking(ADS1292_SSP_PORT, &xferConfig) == Length)
		return 0;

	return 1;
}


/**
=====================================================================
@fn			void CanalInicializarEstructura (t_canal* canal)
@brief 		Inicializo la estructura de adquisición
@author		fede(rouxfederico@gmail.com)
=====================================================================
*/

void ADS1292_CanalInicializarEstructura (t_canal* canal)
{
	canal->ind = 0;												// Inicializo el índice de cuenta
	canal->pAct = canal->buf1;									// Seteo el puntero a buffer activo
	canal->pAlm = canal->buf2;									// Seteo el puntero a buffer de almacenamiento
	canal->pEnv = canal->buf3;									// Seteo el puntero a buffer de envio
	canal->estadoBuffer = BUF_INACTIVO;						// Inicializo el estado del buffer a inactivo
	canal->bufferLlenoPendiente = false;
	return;
}

/**
=====================================================================
@fn 		void CanalIntercambiarPunteros (t_canal* canal)
@brief 		Intercambio los punteros de adquisición y almacenamiento
@author		fede(rouxfederico@gmail.com)
=====================================================================
*/

__inline void ADS1292_CanalIntercambiarPunteros (t_canal* canal)
{
	int32_t* pAux;
	int32_t* pAux2;
/*
	pAux = canal->pAct;
	canal->pAct = canal->pAlm;
	canal->pAlm = pAux;
*/
	pAux = canal->pAlm;
	canal->pAlm = canal->pAct;
	pAux2 = canal->pEnv;
	canal->pEnv = pAux;
	canal->pAct = pAux2;

	return;
}

/**
=====================================================================
@fn 		int32_t ADS1292_InicializarPines(void)
@brief 		Inicializo pines del ads1292 y los pongo en estado normal
@author		fede(rouxfederico@gmail.com)
=====================================================================
*/

int32_t ADS1292_InicializarPines (void) {

	Chip_IOCON_PinMux(LPC_IOCON, ADS1292_CS_PORT, ADS1292_CS_PIN, IOCON_MODE_PULLUP, IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON, ADS1292_DR_PORT, ADS1292_DR_PIN, IOCON_MODE_INACT, IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON, ADS1292_PWDN_PORT, ADS1292_PWDN_PIN, IOCON_MODE_PULLUP, IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON, ADS1292_START_PORT, ADS1292_START_PIN, IOCON_MODE_PULLDOWN, IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON, ADS1292_CLK_PORT, ADS1292_CLK_PIN, IOCON_MODE_PULLDOWN, IOCON_FUNC0);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO, ADS1292_CS_PORT, ADS1292_CS_PIN);			/* chip select */
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, ADS1292_DR_PORT, ADS1292_DR_PIN);			/* data ready*/
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, ADS1292_PWDN_PORT, ADS1292_PWDN_PIN);		/* pwdn/reset */
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, ADS1292_START_PORT, ADS1292_START_PIN);		/* start */
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, ADS1292_CLK_PORT, ADS1292_CLK_PIN);			/* clock */

	/* estado normal de los pines de salida */
	ADS1292_DEASSERT_CS();
	ADS1292_DEASSERT_RESET();
	ADS1292_DEASSERT_START();


	return 0;
}


/*======================================================================================================
 * 									funciones internas
 *======================================================================================================*/

/**
=====================================================================
@fn			ADS1292_ConfigurarCanalSSP
@brief 		Tarea de inicializacion del puerto SSP utilizado por el ADS1292
@author		fede(rouxfederico@gmail.com)
=====================================================================
*/

static int32_t ADS1292_ConfigurarCanalSSP( LPC_SSP_T *pSSP )
{

	/* inicializo pines del bus ads1292 */
	Board_SSP_Init(pSSP);
	ADS1292_InicializarPines();

	Chip_SSP_Init(pSSP);
	Chip_SSP_Enable(pSSP);
	Chip_Clock_SetPCLKDiv(SYSCTL_CLOCK_SSP1, 1);

	Chip_SSP_Set_Mode(pSSP, SSP_MODE_MASTER);
	Chip_SSP_SetFormat(pSSP, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, ADS1292_SPI_SAMPLE_ON_TRAILING_EDGE_CPHA_1|ADS1292_SPI_CLOCK_BASE_VALUE_CPOL_1);
	Chip_SSP_SetBitRate(pSSP, ADS_SSP_SCKL_FREQ);

	InicializarTimer(ADS1292_INTERNAL_CLK_FREQ);

	return 0;

	// aca estaba el manejo de colas de datos, lo borre
}

/*==========================================================================================================
 *	\function	static uint32_t ADS1292_EnviarComandoCorto( uint8_t cComando)
 * 	\brief 		Envío un byte al ADS1292 a través del SSP seleccionado
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *==========================================================================================================*/


static uint32_t ADS1292_EnviarComandoCorto(cmdId_t cmdId)
{
	uint8_t cmd;
	int32_t delay;

	cmd = ads1292_cmdOp[cmdId];
	delay = ads1292_cmdDelay[cmdId];

	if(ADS1292_SSP_Enviar( &cmd, ADS1292_LARGO_CMD_CORTO)) return 1;
	TimerDelayTicks(delay);

	return 0;
}

/*==========================================================================================================
 *	\function	static uint8_t ADS1292_EscribirUnRegistro( uint8_t registro, uint8_t datos)
 * 	\brief 		Envio el comando para escribir un registro
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *==========================================================================================================*/


static uint8_t ADS1292_EscribirUnRegistro( uint8_t registro, uint8_t datos)
{
	uint8_t bufCmd [ADS1292_ESCRIBIR_REG_LARGO];

	bufCmd[0] = (ADS_WREG | registro);
	bufCmd[1] = 0x00;
	bufCmd[2] = datos;

	if(ADS1292_SSP_Enviar( &bufCmd[0], ADS1292_LARGO_CMD_CORTO)) return 1;
	TimerDelayTicks(ADS1292_DELAY_SDECODE);

	if(ADS1292_SSP_Enviar( &bufCmd[1], ADS1292_LARGO_CMD_CORTO)) return 1;
	TimerDelayTicks(ADS1292_DELAY_SDECODE);

	if(ADS1292_SSP_Enviar( &bufCmd[2], ADS1292_LARGO_CMD_CORTO)) return 1;
	TimerDelayTicks(ADS1292_DELAY_SDECODE);

	return 0;

}



/*==========================================================================================================
 *	\function	static uint8_t ADS1292_LeerUnRegistro( uint8_t registro, uint8_t* byteLeido)
 * 	\brief 		Envio el comando para escribir un registro
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *==========================================================================================================*/

static uint8_t ADS1292_LeerUnRegistro( uint8_t registro, uint8_t* byteLeido)
{

	uint8_t cmd;

	cmd = ADS_RREG | registro;
	if(ADS1292_SSP_Enviar(&cmd, ADS1292_LARGO_CMD_CORTO)) return 1;
	TimerDelayTicks(ADS1292_DELAY_SDECODE);

	cmd = 0x00;
	if(ADS1292_SSP_Enviar(&cmd, ADS1292_LARGO_CMD_CORTO)) return 1;
	TimerDelayTicks(ADS1292_DELAY_SDECODE);

	if(ADS1292_SSP_Recibir(byteLeido, ADS1292_LARGO_CMD_CORTO)) return 1;
	TimerDelayTicks(ADS1292_DELAY_SDECODE);

	return 0;
}


/*===================================================================================
 *	\function	void ADS1292_Iniciar_Adquisicion ( void *pvParameters )
 * 	\brief 		Inicio la adquisición en forma contínua del ADS1292
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void ADS1292_Iniciar_Adquisicion ( void )
{

	/* Instalo la tarea que lee lo enviado por el ADS despues del pulso de DRDY */
	ADS1292_InstalarTareaDatosListos();
	/* Inicializo las estructuras de los canales */
	ADS1292_CanalInicializarEstructura(&canal0);
	ADS1292_CanalInicializarEstructura(&canal1);

	/* Activo la comunicación con el ADS mediante SPI */
	ADS1292_EnviarComandoCorto(ADS_SDATAC_ID);
	ADS1292_ASSERT_START();
	//ADS1292_EnviarComandoCorto(ADS_START_ID);
	ADS1292_EnviarComandoCorto(ADS_RDATAC_ID);
	//ADS1292_DEASSERT_CS();

	/* Limpio la interrupción pendiente */
	Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, ADS1292_DR_PORT, ADS1292_DR_PIN);

	/* Habilito las interrupciones generales */
	/* Enable the interrupt and set its priority to the minimum
	interrupt priority.  A separate command can be issued to raise
	the priority if desired. */
	// NVIC_SetPriority( EINT3_IRQn, (configSPI_INTERRUPT_PRIORITY + 1));
	// NVIC_EnableIRQ(ADS1292_DR_EINTx);

	/* Habilito la interrupción del pin de Data Ready */
	ADS1292_ASSERT_CS();
	ADS1292_ACTIVAR_IRQ_DRDY();
	NVIC_EnableIRQ(ADS1292_DR_EINTx);

	return;
}

/*===================================================================================
 *	\function	ADS1292_Inicializar
 * 	\brief 		Funcion de inicializacion del ADS1292
 * 	\author		fede (rouxfederico@gmail.com)
 *===================================================================================*/

int32_t ADS1292_Inicializar (void) {
	uint8_t us_byte_ID = 0x00;

	ADS1292_ConfigurarCanalSSP(ADS_SSP_PORT);


	/* Espero 2^12 pulsos tMOD */
	vTaskDelay(ADS1292_DELAY_INICIAL);

	ADS1292_DEASSERT_START();

	ADS1292_ASSERT_RESET();
	ADS1292_DEASSERT_RESET();

	/* Pongo en alto el pin de CS y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_DEASSERT_CS();

	/* Pongo en bajo el pin de chip select y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_ASSERT_CS();

	/* El ADS se inicia en modo RDATAC, se pasa a modo SDATAC para escribir regs. Se espera 4 tCLK */
	ADS1292_EnviarComandoCorto(ADS_SDATAC_ID);

	/* Leo el ID para controlar que la comunicación esté toda bien : */
	ADS1292_LeerUnRegistro(ADS_ID, &us_byte_ID);
	if(us_byte_ID == ADS1292_ID_CORRECTO)
		ADS1292_inicializado = 1;

	/*  Voy a escribir el registro CONFIG1. Desde esa dirección, solo escribo 1 registro : 125SPS + continuous mode  */
	ADS1292_EscribirUnRegistro( ADS_CONFIG1, ADS_CONFIG_1_OS_RATIO_500SPS);

	//ADS1292_EscribirUnRegistro( ADS_CONFIG2, ADS_CONFIG2_INT_TEST|ADS_CONFIG2_TEST_FREQ);


	/*  Voy a escribir el registro ADS_CH1SET. Desde esa dirección, solo escribo 1 registro : Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida */
	ADS1292_EscribirUnRegistro( ADS_CH1SET, ADS_CH1SET_GAIN_1 | ADS_CH1SET_MUX_NORMAL);

	/*  Voy a escribir el registro ADS_CH2SET. Desde esa dirección, solo escribo 1 registro : Bit 7 en 1 obligatorio, bit 3 conecto CLK al pin de salida */
	ADS1292_EscribirUnRegistro( ADS_CH2SET, ADS_CH2SET_GAIN_1 | ADS_CH2SET_MUX_NORMAL);

	/* Pongo en bajo el pin de chip select y espero 9 pulsos tMOD (tiempo menor a 1ms) y 1s despues del Power-On */
	ADS1292_DEASSERT_CS();

	return 0;
}

/*======================================================================================================
 * 		DEFINICION DE TAREAS Y SU INSTALADOR :
 *======================================================================================================*/

/*===================================================================================
 *	\function	static void ADS1292_Inicializar_Tarea_Datos_Listos (void)
 * 	\brief 		Función de inicialización de la tarea que atiende el pin DRDY
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

int32_t ADS1292_InstalarTareaDatosListos (void)
{
	/* Inicializo la estructura del semáforo para sincronizar buffers completos. Lo tomo para dejarlo listo para usar */
	vSemaphoreCreateBinary(vSemaforoBinBuffersCompletos);
	if(vSemaforoBinBuffersCompletos == NULL) return 1;
	if(xSemaphoreTake(vSemaforoBinBuffersCompletos, portMAX_DELAY) != pdTRUE) return 1;

	/* Inicializo la estructura del semáforo para sincronizar data ready*/
	vSemaphoreCreateBinary(vSemaforoBin_ADS1292_DRDY);
	if(vSemaforoBin_ADS1292_DRDY == NULL) return 1;
	if(xSemaphoreTake(vSemaforoBin_ADS1292_DRDY, portMAX_DELAY) != pdTRUE) return 1;


	/*Creo tarea de datos listos*/
	xTaskCreate (tADS1292_DatosListos,
					 (const int8_t* const) "Datos Listos",
					 DATOS_LISTOS_STACK,
					 NULL,
					 DATOS_LISTOS_PRIORIDAD,
					 (xTaskHandle *) NULL);
	return;
}

/*===================================================================================
 *	\function	static void tADS1292_DatosListos( void *pvParameters )
 * 	\brief 		Tarea de inicializacion del ADS1292. Ver SBAS502B pág. 63
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void tADS1292_DatosListos ( void *pvParameters )
{

	uint8_t msg[40];

	Chip_SSP_DATA_SETUP_T xferConfig;
	t_u32 datoCanal;

	sprintf(msg, "Inicio datos listos \r\n\r\n", canal0.ind);
	UARTEscribirString(pUartBT, msg);
	//ADS1292_ASSERT_CS();

	while(1)
	{
		/* Clavo la tarea esperando a que el pin DRDY se active y la IRQ me de el semáforo */
		if(xSemaphoreTake( vSemaforoBin_ADS1292_DRDY, portMAX_DELAY) == pdTRUE)
		{
			/* Bajo el pin de CS para activar el SSP : */
			empezo_read_ssp_ADS = 1;

			xferConfig.tx_data = NULL;
			xferConfig.tx_cnt  = 0;
			xferConfig.rx_data = ADS1292_Datos_Leidos;
			xferConfig.rx_cnt  = 0;
			xferConfig.length  = ADS1292_RDATAC_CANTBYTESLEIDOS;

			//ADS1292_ASSERT_CS();
			/* Leo todos los bits y chequeo haber leído esa misma cantidad */
			if(Chip_SSP_RWFrames_Blocking(ADS1292_SSP_PORT, &xferConfig) == ADS1292_RDATAC_CANTBYTESLEIDOS)
			{
				/* Levanto el pin de CS para desactivar el SSP : */
				if( ((ADS1292_Datos_Leidos [ADS1292_DATOS_LEIDOS_STATUS_B2] & 0xF0 ) == ADS1292_STATUS_B2) &&
					((ADS1292_Datos_Leidos [ADS1292_DATOS_LEIDOS_STATUS_B1] & 0x1F ) == ADS1292_STATUS_B1) &&
					(ADS1292_Datos_Leidos [ADS1292_DATOS_LEIDOS_STATUS_B0] == ADS1292_STATUS_B0) )
				{

					/* Armo el dato de 24 bits recibido para el canal 0 */
					datoCanal.bytes.b0 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO0_B0];
					datoCanal.bytes.b1 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO0_B1];
					datoCanal.bytes.b2 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO0_B2];
					datoCanal.bytes.b3 = 0x00;

					if(canal0.ind%10 == 0 && enviandoOnline) {
						sprintf(msg, "datosPerdidos:%d\r\n", datosPerdidos);
						UARTEscribirString(pUartBT, msg);
						sprintf(msg, "datosOk:%d\r\n", datosOk);
						UARTEscribirString(pUartBT, msg);
					}

					if(canal0.ind%10 == 0 && enviandoOnline) {
						sprintf(msg, "CH0:%d\r\n", datoCanal.qword);
						UARTEscribirString(pUartBT, msg);

					}

					/* Asigno el dato armado al buffer del canal 0 */
					canal0.pAct[canal0.ind++] = datoCanal.qword;

					/* Pregunto si llegue al final del buffer : */
					if(canal0.ind >= CANAL_BUFFER_L)
					{
						canal0.estadoBuffer = PENDIENTE;
						canal0.bufferLlenoPendiente = true;
						canal0.ind = 0;
						ADS1292_CanalIntercambiarPunteros(&canal0);
					}



					/* Armo el dato de 24 bits recibido para el canal 1 */
					datoCanal.bytes.b0 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO1_B0];
					datoCanal.bytes.b1 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO1_B1];
					datoCanal.bytes.b2 = ADS1292_Datos_Leidos[ADS1292_DATOS_LEIDOS_DATO1_B2];
					datoCanal.bytes.b3 = 0x00;

					if(canal1.ind%10 == 0 && enviandoOnline) {
						sprintf(msg, "CH1:%d\r\n", datoCanal.qword);
						UARTEscribirString(pUartBT, msg);
					}

					/* Asigno el dato armado al buffer del canal 1 */
					canal1.pAct[canal1.ind++] = datoCanal.qword;

					/* Pregunto si llegue al final del buffer : */
					if(canal1.ind >= CANAL_BUFFER_L)
					{
						canal1.bufferLlenoPendiente = true;
						canal1.estadoBuffer = PENDIENTE;
						canal1.ind = 0;
						canal1.ind = 0;
						ADS1292_CanalIntercambiarPunteros(&canal1);
					}



					/* Pregunto si se llenaron los dos buffers */
					if(canal0.bufferLlenoPendiente && canal1.bufferLlenoPendiente)
					{
						/* Bajo los flags de buffer pendiente */
						canal0.bufferLlenoPendiente = false;
						canal1.bufferLlenoPendiente = false;

						/* Doy el semáforo para que el menú lo levante */
						if(xSemaphoreGive(vSemaforoBinBuffersCompletos) != pdTRUE)
						{
							sprintf(msg, "\n\rerror : give sem buf ads completo\r\n");
							UARTEscribirString(pUartBT, msg);
						}
					}
				}
				else
				{
					sprintf(msg, "\n\rerror : Status del ADS\r\n");
					UARTEscribirString(pUartBT, msg);
				}

			}
			else
			{
				// UART_Enviar_Mensaje((const int8_t * const)"\n\rerror : cantidad bytes leidos ADS");
				// configASSERT(0);
				sprintf(msg, "\r\nerror : cantidad bytes leidos ADS\r\n");
				UARTEscribirString(pUartBT, msg);
			}
		}
		else
		{
			// UART_Enviar_Mensaje("\n\rerror : tomar semaforo drdy");
			// configASSERT(0);
		}


	}

	while(1)
	{
		vTaskDelay (1000);
	}
	return;
}

/*===================================================================================
 *	\function	void InstalarEjemploIniciaAdquisicion (void)
 * 	\brief 		Instalo tarea de ejemplo
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void InstalarEjemploIniciaAdquisicion (void)
{
	xTaskCreate (tEjemploIniciaAdquisicion,
				 (const int8_t* const) "Datos Listos",
				 1000,
				 NULL,
				 4,
				 NULL);
	return ;
}

/*===================================================================================
 *	\function		void tEjemploIniciaAdquisicion ( void *pvParameters )
 * 	\brief 		Tarea de ejemplo para iniciar la adquisición
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *===================================================================================*/

void tEjemploIniciaAdquisicion ( void *pvParameters )
{

	while(1)
	{
		if(ADS1292_inicializado && !ADS1292_adquiriendo)
		{
			ADS1292_Iniciar_Adquisicion();
			ADS1292_adquiriendo = 1;
		}
		else
			vTaskDelay (9999);
	}
	return;
}


/*======================================================================================================
 * 		RUTINA DE ATENCIÓN DE INTERRUPCIÓN :
 *======================================================================================================*/

/*======================================================================================================
 *	\function	void EINT3_IRQHandler (void)
 * 	\brief 		Inicializacion de pines del ADS1292
 * 	\author		ROUX, Federico G. (froux@favaloro.edu.ar)
 *======================================================================================================*/

void EINT3_IRQHandler (void)
{

	uint8_t reg_status;
	static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

	/* Leo este reg. para saber si la int. es de P0 o P2 */
	reg_status = LPC_GPIOINT->STATUS;

	/* Analizo si la interrupción es por un pin de P0 o de P2 */
	if(reg_status & GPIOINT_INTSTAT_P0INT)
	{

	}
	else if (reg_status & GPIOINT_INTSTAT_P2INT)
	{

		/* Interrupción en pines de P0, flanco de bajada */
		if(LPC_GPIOINT->IO2.STATF & (1 << ADS1292_DR_PIN))
		{
			/* Limpio la interrupcion pendiente */
			Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, ADS1292_DR_PORT, 1 << ADS1292_DR_PIN);

			/* Cedo el semáforo desde la interrupción para atenderlo desde la tarea */
			if(xSemaphoreGiveFromISR(vSemaforoBin_ADS1292_DRDY, &xHigherPriorityTaskWoken) != pdTRUE)
			{
				// UART_Enviar_Mensaje("\n\rerror : dar semaforo drdy");
				datosPerdidos++;
			}
			else
				datosOk++;
		}
	}
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);


}

