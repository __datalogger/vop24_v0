/*
 * ADS1292.h
 *
 *  Created on: 02/10/2013
 *      Author: froux
 */

#ifndef ADS1292_H_
#define ADS1292_H_

/*==================================================================================
 *		DEFINICION DE CONSTANTES :
 *==================================================================================*/

/** @brief puerto SPI dedicado al ads1292 */
#define ADS1292_SSP_PORT			LPC_SSP1



/* constantes de delays en ms */

/** @brief ADS1292_DELAY_POWERON delay despues de conectar la alimentacion 1seg*/
#define ADS1292_DELAY_POWERON											( 1000 / portTICK_RATE_MS)
/** @brief ADS1292_DELAY_INICIAL delay despues de power on 32 ms (en ms) */
#define ADS1292_DELAY_INICIAL											( 32 / portTICK_RATE_MS)
/** @brief  ADS1292_DELAY_CS delay luego de mover el CS */
#define ADS1292_DELAY_CS												( 1 / portTICK_RATE_MS)

/* constantes de delays en tclk */
/** @brief ADS1292_DELAY_RESET ancho del reset 1 tmod = 4tclk (en tclk)*/
#define ADS1292_DELAY_RESET												4
/** @brief ADS1292_DELAY_POST_RESET delay despues del reset 18tclk*/
#define ADS1292_DELAY_POST_RESET										18
/** @brief ADS1292_DELAY_SDATAC */
#define ADS1292_DELAY_SDATAC											4
/** @brief ADS1292_DELAY_SDECODE delay entre bytes de un comando (depende de fclk y fsck) va desde 4 a 0 */
#define ADS1292_DELAY_SDECODE											0


#define ADS1292_DELAY_REFBUF											( 10 / portTICK_RATE_MS)
#define ADS1292_DELAY_CMD												( 1 / portTICK_RATE_MS)

#define ADS1292_BUFFER_TAM_BYTES										( 32 )
#define ADS1292_RX_TIMEOUT												( 5000UL )
#define ADS1292_TX_TIMEOUT												( 5000UL )

#define ADS1292_MUTEX_TOUT												( ( void * ) ( 500UL / portTICK_RATE_MS ) )
#define ADS1292_LARGO_CMD_CORTO											( 1 )
#define ADS1292_LARGO_CMD_LARGO											( 3 )
#define ADS1292_ESCRIBIR_REG_LARGO										( 3 )
#define ADS1292_LEER_REG_LARGO											( 2 )

/* constantes relacionadas con el ADS1292 */
#define ADS1292_BYTES_POR_PALABRA										3						// 24 bits de dato
#define ADS1292_PALABRAS_POR_ADQ										3						// Status + CH1 + Ch2
#define ADS1292_BYTES_POR_ADQ											(ADS1292_PALABRAS_POR_ADQ * ADS1292_BYTES_POR_PALABRA)

#define FREQ_TEST_SGN													1						// Frecuencia de la señal de prueba
#define	FS																500						// 125 SPS

/* Definiciones del puerto SSP utilizado */

#define ADS1292_INTERNAL_CLK_FREQ										( 512000 )

#define ADS1292_SSP_PCLK_DIV											4												/* Divisor del PCLK */
#define ADS1292_SSP_CR0_SCR												0x07											/* Preescaler del clock del registro CR0 (UM10360 pág. 431) */
// #define ADS1292_SSP_FRECUENCIA											(2 * ADS1292_INTERNAL_CLK_FREQ)					/* Frecuencia del SSP1 */
#define ADS_SSP_SCKL_FREQ												(512000)

#define ADS1292_SSP_CPSR_CPSDVSR										(SystemCoreClock / (ADS1292_SSP_FRECUENCIA * ADS1292_SSP_PCLK_DIV * (ADS1292_SSP_CR0_SCR + 1)))

/*******************************************************************************
 * ADS1292 Pines y Conexionado
 ******************************************************************************/

/** @brief puerto spi conectado al ads1292*/
#define ADS_SSP_PORT LPC_SSP1

/** @brief cs chip select del spi: se maneja como GPIO */
#define ADS1292_CS_PORT								0								/* Puerto de Chip Select */
#define ADS1292_CS_PIN								6								/* Pin de Chip Select */

/** @brief drdy pin que avisa cuando se termino de tomar una muestra */
#define ADS1292_DR_PORT								2								/* Port de Data Ready */
#define ADS1292_DR_PIN								0								/* Pin de Data Ready */
#define ADS1292_DR_EINTx							EINT3_IRQn

/** @brief pwdn/reset pin para reiniciar o apagar el ads1292*/
#define ADS1292_PWDN_PORT							0
#define ADS1292_PWDN_PIN							4
#define ADS1292_PWDN_EST_NORMAL						true
#define ADS1292_PWDN_EST_RESET						false
#define ADS1292_PWDN_TRESET_HOLD					2^9*fmodclk			// fixme revisar el tiempo que va
#define ADS1292_PWDN_TRESET_INIT					18*tclk				// fixme revisar el tiempo que va

/** @brief ads1292 start se puede usar para controlar el inicio del muestreo por un pin */
#define ADS1292_CLK_PORT							1
#define ADS1292_CLK_PIN								0

/** @bruef ads1292 clk pin: si se usa el clk interno, este pin queda en HiZ o sacar el clk hacia afuera segun CONFIG2 */
#define ADS1292_START_PORT							0
#define ADS1292_START_PIN							5



#define ADS1292_ASSERT_START()						Chip_GPIO_SetPinState(LPC_GPIO, ADS1292_START_PORT, ADS1292_START_PIN, true)
#define ADS1292_DEASSERT_START()					Chip_GPIO_SetPinState(LPC_GPIO, ADS1292_START_PORT, ADS1292_START_PIN, false)

#define ADS1292_ASSERT_RESET()						Chip_GPIO_SetPinState(LPC_GPIO, ADS1292_PWDN_PORT, ADS1292_PWDN_PIN, false); \
													TimerDelayTicks(ADS1292_DELAY_RESET)

#define ADS1292_DEASSERT_RESET()					Chip_GPIO_SetPinState(LPC_GPIO, ADS1292_PWDN_PORT, ADS1292_PWDN_PIN, true); \
													vTaskDelay(ADS1292_DELAY_POST_RESET);

#define ADS1292_DEASSERT_CS()						Chip_GPIO_SetPinState(LPC_GPIO, ADS1292_CS_PORT, ADS1292_CS_PIN, true); \
													vTaskDelay(ADS1292_DELAY_CS)

#define ADS1292_ASSERT_CS()							Chip_GPIO_SetPinState(LPC_GPIO, ADS1292_CS_PORT, ADS1292_CS_PIN, false); \
													vTaskDelay(ADS1292_DELAY_CS)

#define ADS1292_ACTIVAR_IRQ_DRDY()					Chip_GPIOINT_SetIntFalling(LPC_GPIOINT,  ADS1292_DR_PORT, 1 << ADS1292_DR_PIN)
// corregir porque baja todas las irq de los pines
#define ADS1292_DESACTIVAR_IRQ_DRDY()				Chip_GPIOINT_SetIntFalling(LPC_GPIOINT, ADS1292_DR_PORT, 1 << ADS1292_DR_PIN)

/*******************************************************************************
 * ADS1292 SSP/SPI port configuration definitions.
 ******************************************************************************/

#define ADS1292_SSP_DATABIT_8 								( SSP_DATABIT_8 )
#define ADS1292_SPI_SAMPLE_ON_LEADING_EDGE_CPHA_0 			( SSP_CPHA_FIRST )
#define ADS1292_SPI_SAMPLE_ON_TRAILING_EDGE_CPHA_1 			( SSP_CPHA_SECOND )
#define ADS1292_SPI_CLOCK_BASE_VALUE_CPOL_0					( SSP_CPOL_LO ) /* Looks strange, but is in accordance with the comments in the NXP code, which even says in the code it is not a bug so must get questioned often. */
#define ADS1292_SPI_CLOCK_BASE_VALUE_CPOL_1					( SSP_CPOL_HI ) /* Looks strange, but is in accordance with the comments in the NXP code, which even says in the code it is not a bug so must get questioned often. */
#define ADS1292_SPI_MASTER_MODE 							( SSP_MASTER_MODE )
#define ADS1292_SSP_FRAME_SPI 								( SSP_CR0_FRF_SPI )
#define ADS1292_DEFAULT_READ_MUTEX_TIMEOUT					( ( portTickType ) 1000 )


/*=============================================================
 * Códigos de operación del ADS1292. Ver SBAS502B pág. 35
 *=============================================================*/

/* System Commands : */
#define 	ADS_WAKEUP  			0x02								// Wake-up from standby mode
#define		ADS_STANDBY				0x04								// Enter standby mode
#define		ADS_RESET 				0x06								// Reset the device
#define 	ADS_START 				0x08								// Start or restart (synchronize) conversions
#define 	ADS_STOP 				0x0A								// Stop conversion
#define 	ADS_OFFSETCAL 			0x1A								// Channel offset calibracion

/* Data Read Commands : */
#define		ADS_RDATAC				0x10								// Enable Read Data Continuous mode. This mode is the default mode at power-up
#define		ADS_SDATAC				0x11								// Stop Read Data Continuously mode
#define 	ADS_RDATA				0x12								// Read data by command; supports multiple read back

/* Register Read Commands : */
#define		ADS_RREG				0x20								// Read n nnnn registers starting at address r rrrr (001r rrrr - 000n nnnn)
#define 	ADS_WREG 				0x40								// Write n nnnn registers starting at address r rrrr (010r rrrr - 000n nnnn)

/* delays post cada comando : */
#define 	ADS_WAKEUP_DELAY		4
#define		ADS_STANDBY_DELAY		0
#define		ADS_RESET_DELAY			(9*4)
#define 	ADS_START_DELAY			0
#define 	ADS_STOP_DELAY			0
#define 	ADS_OFFSETCAL_DELAY		0
#define		ADS_RDATAC_DELAY		4
#define		ADS_SDATAC_DELAY		4
#define 	ADS_RDATA_DELAY			0
#define		ADS_RREG_DELAY			0
#define 	ADS_WREG_DELAY			0

/* delays post cada comando : */
typedef enum {ADS_WAKEUP_ID, ADS_STANDBY_ID, ADS_RESET_ID, ADS_START_ID, ADS_STOP_ID,
	ADS_OFFSETCAL_ID, ADS_RDATAC_ID, ADS_SDATAC_ID, ADS_RDATA_ID, ADS_RREG_ID, ADS_WREG_ID} cmdId_t;


/*===================================================================
 * Mapa de registros del ADS1292. Ver SBAS502B pág. 39 Tabla 14
 *===================================================================*/

/* Device Settings (Read-Only Registers) : */
#define		ADS_ID					0x00

/* Global Settings Across Channels : */
#define		ADS_CONFIG1   			0x01
#define		ADS_CONFIG2   			0x02
#define		ADS_LOFF 	  			0x03

/* Channel-Specific Settings : */
#define		ADS_CH1SET 		  		0x04
#define		ADS_CH2SET 		  		0x05
#define		ADS_RLD_SENS  			0x06
#define		ADS_LOFF_SENS		  	0x07
#define		ADS_LOFF_STAT 			0x08

/* GPIO and Other Registers : */
#define		ADS_RESP1 	  			0x09
#define		ADS_RESP2     			0x0A
#define		ADS_GPIO 			  	0x0B

#define 	DR_LECTURA				0x00

/**************************************************************
 ** Descripción de los registros :
 **************************************************************/

// Registro ID : ID Control Register (Factory-Programmed, Read-Only) SBAS502B Pág. 39
// Direccion = 00h
// Bit 4 siempre en 1
// Bit [3:2] siempre en 0

#define ADS_ID_BITS_1							(0x01 << 4)

#define ADS_ID_REV_ID_A_MAS						(0x07 << 5)
	#define ADS_ID_REV_ID_A_ADS1x9x				((0x02 << 5) | ADS_ID_BITS_1)
	#define ADS_ID_REV_ID_A_ADS1292R			((0x03 << 5) | ADS_ID_BITS_1)

#define ADS_ID_REV_ID_B_MAS						((0x03 << 0) | ADS_ID_BITS_1)
	#define ADS_ID_REV_ID_B_ADS1191				((0x00 << 0) | ADS_ID_BITS_1)
	#define ADS_ID_REV_ID_B_ADS1192				((0x01 << 0) | ADS_ID_BITS_1)
	#define ADS_ID_REV_ID_B_ADS1291				((0x02 << 0) | ADS_ID_BITS_1)
	#define ADS_ID_REV_ID_B_ADS1292x			((0x03 << 0) | ADS_ID_BITS_1)

#define ADS1292_ID_CORRECTO 					(ADS_ID_BITS_1 | ADS_ID_REV_ID_A_ADS1x9x | ADS_ID_REV_ID_B_ADS1292x)


// Registro CONFIG1 : Configuration Register 1 SBAS502B Pág. 40
// Direccion = 01h
// Bit [6:3] Debe setearse a 0 siempre

#define ADS_CONFIG1_SINGLE_SHOT					(0x01 << 7)					// Modo Single-Shot
#define ADS_CONFIG1_CONTINUOUS				  	(~(0x01 << 7))				// Modo contínuo de conversión

#define	ADS_CONFIG_1_OS_RATIO_MAS				(0x07 << 0)					// These bits determine the oversampling ratio of both channel 1 - 2
	#define	ADS_CONFIG_1_OS_RATIO_125SPS		(0x00 << 0)					// fMOD / 1024 - 125SPS
	#define	ADS_CONFIG_1_OS_RATIO_250SPS		(0x01 << 0)					// fMOD / 512 - 250SPS
	#define	ADS_CONFIG_1_OS_RATIO_500SPS		(0x02 << 0)					// fMOD / 256 - 500SPS
	#define	ADS_CONFIG_1_OS_RATIO_1kSPS			(0x03 << 0)					// fMOD / 128 - 1kSPS
	#define	ADS_CONFIG_1_OS_RATIO_2kSPS			(0x04 << 0)					// fMOD / 64 - 2kSPS
	#define	ADS_CONFIG_1_OS_RATIO_4kSPS			(0x05 << 0)					// fMOD / 32 - 4kSPS
	#define	ADS_CONFIG_1_OS_RATIO_8kSPS			(0x06 << 0)					// fMOD / 16 - 8kSPS

// Registro CONFIG2 : Configuration Register 2 SBAS502B Pág. 41
// Direccion = 02h
// Bit 7 siempre en 1
// Bit 2 siempre en 0

#define ADS_CONFIG2_BITS_1						(0x01 << 7)								// Bits obligatorios en 1
#define ADS_CONFIG2_PDB_LOFF_COMP				((0x01 << 6) | ADS_CONFIG2_BITS_1)		// Lead-off comparator enabled
#define ADS_CONFIG2_PDB_REFBUF					((0x01 << 5) | ADS_CONFIG2_BITS_1)		// Reference buffer is enabled
#define ADS_CONFIG2_VREF_4V						((0x01 << 4) | ADS_CONFIG2_BITS_1)		// 1 - 4.033V reference // 0 - 2.42V reference
#define ADS_CONFIG2_CLK_EN						((0x01 << 3) | ADS_CONFIG2_BITS_1)		// Oscillator clock output enabled (salida del CLK pin)
#define ADS_CONFIG2_INT_TEST					((0x01 << 1) | ADS_CONFIG2_BITS_1)		// Test signal selection : 0 :off // 1 = On; amplitude = +- (VREFP - VREFN)/2400
#define ADS_CONFIG2_TEST_FREQ					((0x01 << 0) | ADS_CONFIG2_BITS_1)		// Test signal freq. 0 : DC // 1 : Square wave 1 Hz


// Registro CH1SET : Channel 1 Settings. SBAS502B Pág. 43
// Dirección = 04h

#define ADS_CH1SET_PD 							(0x01 << 7)					// 1 : Channel 1 power-down (setear mux a corto) // 0 : Normal operation
#define ADS_CH1SET_GAIN_MAS						(0x07 << 4)					// Channel 1 PGA gain setting
	#define ADS_CH1SET_GAIN_6					(0x00 << 4)					// Ganancia = 6
	#define ADS_CH1SET_GAIN_1					(0x01 << 4)					// Ganancia = 1
	#define ADS_CH1SET_GAIN_2					(0x02 << 4)					// Ganancia = 2
	#define ADS_CH1SET_GAIN_3					(0x03 << 4)					// Ganancia = 3
	#define ADS_CH1SET_GAIN_4					(0x04 << 4)					// Ganancia = 4
	#define ADS_CH1SET_GAIN_8					(0x05 << 4)					// Ganancia = 8
	#define ADS_CH1SET_GAIN_12					(0x06 << 4)					// Ganancia = 12

#define ADS_CH1SET_MUX_MAS						(0x0F << 0)					// Channel 1 input selection
	#define ADS_CH1SET_MUX_NORMAL				(0x00 << 0)					// Normal electrode input (default)
	#define ADS_CH1SET_MUX_SHORTED				(0x01 << 0)					// Input shorted (for offset measurements)
	#define ADS_CH1SET_MUX_RDL_MEAS				(0x02 << 0)					// RLD_MEASURE
	#define ADS_CH1SET_MUX_MVDD					(0x03 << 0)					// MVDD for supply measurements = (AVDD + AVSS)/2 (setear PGA1 para no saturar)
	#define ADS_CH1SET_MUX_TEMP_SENS			(0x04 << 0)					// Temperature sensor
	#define ADS_CH1SET_MUX_TEST_SIGNAL			(0x05 << 0)					// Test signal
	#define ADS_CH1SET_MUX_RLD_DRP				(0x06 << 0)					// RLD_DRP (positive input is connected to RLDIN)
	#define ADS_CH1SET_MUX_RLD_DRM				(0x07 << 0)					// RLD_DRM (negative input is conected to RLDIN)
	#define ADS_CH1SET_MUX_RLD_DRPM				(0x08 << 0)					// RLD_DRPM (both positive and negative inputs are connected to RLDIN)
	#define ADS_CH1SET_MUX_IN3					(0x09 << 0)					// Route IN3P and IN3N to channel 1 inputs

// Registro CH2SET : Channel 2 Settings. SBAS502B Pág. 44
// Dirección = 05h

#define ADS_CH2SET_PD 							(0x01 << 7)					// 1 : Channel 1 power-down (setear mux a corto) // 0 : Normal operation
#define ADS_CH2SET_GAIN_MAS						(0x07 << 4)					// Channel 1 PGA gain setting
	#define ADS_CH2SET_GAIN_6					(0x00 << 4)					// Ganancia = 6
	#define ADS_CH2SET_GAIN_1					(0x01 << 4)					// Ganancia = 1
	#define ADS_CH2SET_GAIN_2					(0x02 << 4)					// Ganancia = 2
	#define ADS_CH2SET_GAIN_3					(0x03 << 4)					// Ganancia = 3
	#define ADS_CH2SET_GAIN_4					(0x04 << 4)					// Ganancia = 4
	#define ADS_CH2SET_GAIN_8					(0x05 << 4)					// Ganancia = 8
	#define ADS_CH2SET_GAIN_12					(0x06 << 4)					// Ganancia = 12

#define ADS_CH2SET_MUX_MAS						(0x0F << 0)					// Channel 1 input selection
	#define ADS_CH2SET_MUX_NORMAL				(0x00 << 0)					// Normal electrode input (default)
	#define ADS_CH2SET_MUX_SHORTED				(0x01 << 0)					// Input shorted (for offset measurements)
	#define ADS_CH2SET_MUX_RDL_MEAS				(0x02 << 0)					// RLD_MEASURE
	#define ADS_CH2SET_MUX_VDD_2				(0x03 << 0)					// VDD/2 for supply measurements
	#define ADS_CH2SET_MUX_TEMP_SENS			(0x04 << 0)					// Temperature sensor
	#define ADS_CH2SET_MUX_TEST_SIGNAL			(0x05 << 0)					// Test signal
	#define ADS_CH2SET_MUX_RLD_DRP				(0x06 << 0)					// RLD_DRP (positive input is connected to RLDIN)
	#define ADS_CH2SET_MUX_RLD_DRM				(0x07 << 0)					// RLD_DRM (negative input is conected to RLDIN)
	#define ADS_CH2SET_MUX_RLD_DRPM				(0x08 << 0)					// RLD_DRPM (both positive and negative inputs are connected to RLDIN)
	#define ADS_CH2SET_MUX_IN3					(0x09 << 0)					// Route IN3P and IN3N to channel 1 inputs


/**************************************************************
 ** Frecuencias y tiempos de espera tras comandos :
 **************************************************************/

#define ADS_FMOD					128000							// Frecuencia de muestreo al inicializar con clock interno
#define ADS_TPOR					4096							// Tiempo de espera después de encender en tMODs. SBAS502B Pág. 62 Tabla 17
#define ADS_TRST					1								// Ancho del pulso de reset en tiempo de tMODs. SBAS502B Pág. 62 Tabla 17
#define ADS_TDRST					18								// Tiempo de espera después del pulso de reset en ciclos de clock tCLK
#define ADS_CS_SOSTENER				4								// Tiempo de mantener CS en bajo después de mandar un comando en tCLK
#define ADS_CMD_RESET_TESP			9								// Tiempo de espera luego de enviar el Reset en tMOD


/*************************************************
 ** Bits del registro IOnIntEnF - Usado en pin DRDY
 *************************************************/

#define GPIOINT_IONINTENF_P00EF						(0x01 << 0)			// Bit para habilitar interrupcion en flanco desc. del P0.0

/*************************************************
 ** Bits del registro IntStat - Usado en pin DRDY
 *************************************************/

#define GPIOINT_INTSTAT_P0INT							(0x01 << 0)						// Interrupcion pendiente en P0
#define GPIOINT_INTSTAT_P2INT							(0x01 << 2)						// Interrupcion pendiente en P2
#define GPIOINT_INTSTAT_P00FEI							(0x01 << 0)						// Interrupcion pendiente en flanco desc. del P0.0
#define GPIOINT_INTCLR_P00CI							(0x01 << 0)						// Bit para limpiar la irq. pendiente

#define GPIOINT_INTSTAT_P211REI							(0x01 << 11)					// Interrupcion pendiente en flanco desc. del P0.0
#define GPIOINT_INTCLR_P211CI							(0x01 << 11)					// Bit para limpiar la irq. pendiente
#define GPIOINT_IO2INTENR_P211							(0x01 << 11)

/*===================================================
 * DATA OUTPUT : Ver SBAS502B pág. 29
 *===================================================*/

/* Cantidad de bytes de un dato del ADS */
#define ADS1292_LARGO_DATO									( 3 )
#define ADS1292_CANT_DATO									( 2 )
#define ADS1292_BYTES_DATO									( ADS1292_LARGO_DATO * ADS1292_CANT_DATO )

#define ADS1292_LARGO_STATUS								( 3 )
#define ADS1292_CANT_STATUS 								( 1 )
#define ADS1292_BYTES_STATUS								( ADS1292_LARGO_STATUS * ADS1292_CANT_STATUS )

/* Cantidad de bytes leidos cada vez que el ADS1292 indica DRDY :
 * Esta cantidad de bytes no depende de si tengo uno o los dos
 * canales activados. Si uno de los dos canales está inactivo
 * simplemente el ADS1292 envía ceros como si fuera lo que está
 * midiendo */

// #define ADS1292_RDATAC_CANTBYTESLEIDOS						( ADS1292_BYTES_STATUS + ADS1292_BYTES_DATO )
#define ADS1292_RDATAC_CANTBYTESLEIDOS						12//( ADS1292_BYTES_STATUS + ADS1292_BYTES_DATO )

#define ADS1292_DATOS_LEIDOS_STATUS_B2						0
#define ADS1292_DATOS_LEIDOS_STATUS_B1						1
#define ADS1292_DATOS_LEIDOS_STATUS_B0						2

#define ADS1292_DATOS_LEIDOS_DATO0_B2						3
#define ADS1292_DATOS_LEIDOS_DATO0_B1						4
#define ADS1292_DATOS_LEIDOS_DATO0_B0						5

#define ADS1292_DATOS_LEIDOS_DATO1_B2						6
#define ADS1292_DATOS_LEIDOS_DATO1_B1						7
#define ADS1292_DATOS_LEIDOS_DATO1_B0						8

/* Formato de los bits de status :
 * (1100 + LOFF_STAT[4:0] + GPIO[1:0] + 0000000000000 )*/
#define ADS1292_STATUS_B2									( 0xC0 )
#define ADS1292_STATUS_B1									( 0x00 )
#define ADS1292_STATUS_B0									( 0x00 )


/*************************************************
 ** Macros para el manejo de CS.
 *************************************************/

#define ADS_SPI_CS_INACTIVO()		LPC_GPIO0->FIOSET0 |= 0x01UL << 6
#define ADS_SPI_CS_ACTIVO()			LPC_GPIO0->FIOCLR0 |= 0x01UL << 6

/****************************************************************************
 ** 						DEFINICIONES DE TIPOS							*
 ****************************************************************************/

typedef struct
{
	uint8_t b0;
	uint8_t b1;
	uint8_t b2;
	uint8_t b3;
} t_4bytes;

typedef struct
{
	int16_t w0;
	int16_t w1;
}t_2words;

typedef volatile union
{
	t_4bytes bytes;
	t_2words words;
	int32_t qword;
} t_u32;

/* Tamaño en bytes de un dato */
#define CANAL_BUFFER_TAM 						4
/* Definición de tipo de dato de adquisición : */
#define CANAL_BUFFER_L							128


typedef enum {BUF_INACTIVO, PENDIENTE, PROCESANDO, FINALIZADO} estadoBuffer_t;

typedef struct
{
	/* Defino un doble buffer para adquisición.  */
	int32_t buf1 [CANAL_BUFFER_L];
	int32_t buf2 [CANAL_BUFFER_L];
	int32_t buf3 [CANAL_BUFFER_L];

	/* Indice de adquisición */
	int32_t ind;
	/* Indice de envio */
	int32_t ind_envio;

	/* Defino puntero a buffer activo, a buffer de almacenamiento y buffer de envío: */
	int32_t* pAct;
	int32_t* pAlm;
	int32_t* pEnv;

	/* flag para saber si hay un buffer lleno esperando a almacenarse */
	estadoBuffer_t estadoBuffer;
	bool bufferLlenoPendiente;
}t_canal;

/*======================================================================================================
 * 									FUNCIONES GLOBALES :
 *======================================================================================================*/

extern void tCanal_Inicializar_Estructura (t_canal* canal);
// extern void tCanal_Intercambiar_Punteros (t_canal* canal);
extern void ADS1292_Iniciar_Adquisicion ( void );
/*==================================================================================
 *		LISTA DE TAREAS Y SU INSTALADOR :
 *==================================================================================*/

/* Tarea de handler del pin DRDY del ADS1292 */
extern int32_t ADS1292_InstalarTareaDatosListos ( void );
extern void tADS1292_DatosListos ( void *pvParameters );

/* Tarea de ejemplo para disparar la adquisición : */
extern void InstalarEjemploIniciaAdquisicion (void);
extern void tEjemploIniciaAdquisicion ( void *pvParameters );

extern int32_t ADS1292_InicializarPines (void);
extern int32_t ADS1292_Inicializar (void);

/*==================================================================================
 *		VARIABLES GLOBALES :
 *==================================================================================*/

/* Array de datos en crudo recibidos por el ADS1292 */
extern uint8_t ADS1292_Datos_Leidos [ADS1292_RDATAC_CANTBYTESLEIDOS];

/* Flags del ADS1292 */
extern volatile uint8_t ADS1292_inicializado;
extern volatile uint8_t ADS1292_adquiriendo;
/* Estructuras de canales de adquisición : */
extern t_canal canal0;
extern t_canal canal1;

/* Semáforos sincronizados con tareas de afuera : */
extern xSemaphoreHandle vSemaforoBinBuffersCompletos;

extern uint8_t datosPerdidos;
extern uint32_t datosOk;


#endif /* ADS1292_H_ */
