BW_OUTDIR=./bw-output
CHIP_LIB_PATH=./src/lpc_chip_175x_6x/Debug
BOARD_LIB_PATH=./src/lpc_board_nxp_lpcxpresso_1769/Debug
SRC_PATH=./src/vop24/Debug
TEST_BUILD_PATH=./test/build/

.PHONY: build test doc sonar clean


build:
	make -C $(CHIP_LIB_PATH)
	make -C $(BOARD_LIB_PATH)
	make -C $(SRC_PATH)

test:
	ceedling test:all gcov:all utils:gcov
    
doc:
	doxygen doxyfile	  
    
sonar:
	build-wrapper-linux-x86-64 --out-dir $(BW_OUTDIR) make proy
	sonar-scanner
	
clean:
	make -C $(CHIP_LIB_PATH) clean
	make -C $(BOARD_LIB_PATH) clean
	make -C $(SRC_PATH) clean
	rm -rf $(BW_OUTDIR)
	rm -rf $(BUILD_PATH)
	rm -rf .scannerwork
