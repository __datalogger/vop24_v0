/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
 
//-- unity: unit test framework
#include "unity.h"
#include "cmock.h"
 
//-- module being tested
//   TODO

#ifdef MOCKEAR
/*
#include "mock_board.h"

#include "mock_FreeRTOS.h"

#include "mock_task.h"
#include "mock_queue.h"
#include "mock_semphr.h"

#include "mock_uart.h"

#include "mock_sapi_sdcard.h"

#include "mock_ff.h"
#include "mock_vop24v0_board.h"

#include "mock_ads1292.h"
#include "mock_timers.h"

#include "mock_clock_17xx_40xx.h"
*/
#else 


#include "projdefs.h"
#include "portmacro.h"
#include "portable.h"

#include "board.h"
#include "FreeRTOS.h"

#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "uart.h"

#include "sapi_sdcard.h"

#include "ff.h"
#include "vop24v0_board.h"

#include "ads1292.h"
#include "timers.h"

#include "clock_17xx_40xx.h"


#endif
#include "apps.h"


 
/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/
 
/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/
 
/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/
 
 
/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/
 
 
/*******************************************************************************
 *    SETUP, TEARDOWN
 ******************************************************************************/
 
void setUp(void)
{
}
 
void tearDown(void)
{
}
 
/*******************************************************************************
 *    TESTS
 ******************************************************************************/
 
void test_CifraAsciiEntero(void) {

    TEST_ASSERT_EQUAL(CifraAsciiEntero('3'), 3);

}
