
# Proyecto : VOP24

## Resumen : firmware del proyecto vop24

### Autor : fede (rouxfederico@gmail.com)

### IDE: MCUXpresso / C

### Instalacion del repo:

Crear una carpeta fuera de la carpeta del repositorio para el workspace (los archivos del workspace no están gitignoreados y ensucian el repo). 

**Agregar los proyectos (2 de lpcopen + vop24) del repo sin tildar la opcion "Copy projects into the Workspace"**


### Documentacion

El proyecto esta documentado con Doxygen. Para generar la documentacion es necesario tener instalado lo sgte:

*    Doxygen : instalar con doxywizard para facilitar la configuracion

        git clone https://github.com/doxygen/doxygen.git
		cd doxygen
		mkdir build
		cd build
		cmake -G "Unix Makefiles" ..
		cmake -Dbuild_wizard=YES ..
        make
        make install

*    Graphviz para graficar la relacion entre funciones

        sudo apt-get install graphviz

### Compilacion desde terminal de linux

Una vez instalado MCUXpresso, es necesario tener agregadas al path las carpetas donde se guarda el compilador y librerias de la IDE, para luego poder compilar desde la terminal. Para ello, añadir las siguientes lineas a ~/.bashrc:

        export PATH=$PATH:/usr/local/mcuxpressoide/ide/tools/bin
        export PATH=$PATH:/usr/local/mcuxpressoide/ide/tools/arm-none-eabi/lib

Luego ejecutar:

        source ~/.bashrc
